<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use SmoDav\Mpesa\C2B\Registrar;
use SmoDav\Mpesa\C2B\Simulate;
use GuzzleHttp\Client;
use SmoDav\Mpesa\Engine\Core;
use SmoDav\Mpesa\Native\NativeCache;
use SmoDav\Mpesa\Native\NativeConfig;
use SmoDav\Mpesa\C2B\STK;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Pmt;
use App\Repository\CallbackRepository;
use App\Repository\PmtRepository;
use App\Service\SendSms;

class MPesaManager extends AbstractController
{

    private $pmtRepo;
    private $cbRepo;
    private $sendSms;
    const CUSTOMER_BUYGOODS_ONLINE = 'CustomerBuyGoodsOnline';

    public function __construct(PmtRepository $pmtRepo, CallbackRepository $cbRepo, SendSms $sendSms){
        $this->pmtRepo = $pmtRepo;
        $this->cbRepo = $cbRepo;
        $this->sendSms = $sendSms;
    }    


    public function auth(): Response
    {
        $config = new \SmoDav\Mpesa\Native\NativeConfig();
        $cache = new NativeCache($config->get('cache_location'));
        $core = new Core(new Client, $config, $cache);
        $authenticator = new \SmoDav\Mpesa\Auth\Authenticator($core);
        $auth = $authenticator->authenticate();
        return $auth;
    }

    public function reg($conf, $val): Response
    {
        $conf = 'https://new.conservatoire.co.ke/mpesa/confirm?secret=secretkey';
        $val = 'https://new.conservatoire.co.ke/mpesa/validate?secret=secretkey';

        $response = Registrar::register(600000)
            ->onConfirmation($conf)
            ->onValidation($val)
            ->submit();

        return $response;
    }

    public function pushSimulate($amt, $number, $ref): Response
    {
        $config = new \SmoDav\Mpesa\Native\NativeConfig();
        $cache = new NativeCache($config->get('cache_location'));
        $core = new Core(new Client, $config, $cache);
        
        $simulate = new Simulate($core);
        $response = $simulate->request($amt)
        ->from($this->formatPhoneNumber($number))
        ->usingReference($ref)
        ->setCommand('CustomerBuyGoodsOnline')
        ->push();

        return $response;

    }

    public function pushSTK($amt, $number, $ref, $desc)
    {
        $config = new NativeConfig();
        $cache = new NativeCache($config->get('cache_location'));
        $core = new Core(new Client, $config, $cache);
        
        $stk = new STK($core);
        $response = $stk->request($amt)
        ->from($this->formatPhoneNumber($number))
        ->usingReference($ref, $desc)
        ->setCommand('CustomerBuyGoodsOnline')
        ->push();

        $data = $response;
        $errors = [];
        if (isset($data->errorMessage)) {
            if($data->errorMessage == "error on exit from activity, no matching transition"){
                $msg = "Number is invalid. Please use an M-pesa activated number.";
                $errors['error'] = $msg;
            } else {
                $msg = $data->errorMessage;
                $errors['error'] = $msg;
            }
            $ret = ['failed', $msg];

            return $ret;

        } else {

            $pmt = new Pmt();
            $pmt->setMerchantrequestid($data->MerchantRequestID);
            $pmt->setCheckoutrequestid($data->CheckoutRequestID);
            $pmt->setResponsecode($data->ResponseCode);
            $pmt->setResponsedescription($data->ResponseDescription);
            $pmt->setCustomermessage($data->CustomerMessage);
            $this->save($pmt);
    
            $ret = ['pending', $data->CheckoutRequestID];

            return $ret;
        
        }


    }

    public function checkStatus($request_id, $purpose) {

        $config = new \SmoDav\Mpesa\Native\NativeConfig();
        $cache = new NativeCache($config->get('cache_location'));
        $core = new Core(new Client, $config, $cache);
        
        $stk = new STK($core);
        $response = $stk->validate($request_id);

        $status = $response;
        
        // if is error
        if (isset($status->errorMessage)) {
            
            $msg = $status->errorMessage;
            $errors['error'] = $msg;
        
            $ret = ['failed', $msg];
            return $ret;
        } else { // if is not error

            $pmt = $this->pmtRepo->findOneByCheckoutrequestid($request_id);
            $pmt->setResultcode($status->ResultCode);
            $pmt->setResultdesc($status->ResultDesc);
            $this->save($pmt);

            if($status->ResultCode == 0){

                $callback = $this->cbRepo->findOneByCheckoutRequestID($request_id);
                
                if(null == $callback){
                    $ret = ['try_again', 'Wait for confirmation message and try again'];
                    return $ret;
                } else {
                    $json = $callback->getCallbackmetadata();
        
                    $Amount = $this->getVar($json, 'Amount', 3);
                    $MpesaReceiptNumber = $this->getVar($json, 'MpesaReceiptNumber', 3);
                    $TransactionDate = $this->getVar($json, 'TransactionDate', 3);
                    $PhoneNumber = $this->getVar($json, 'PhoneNumber', 3);
                
                    $callback->setMpesaReceiptNumber($MpesaReceiptNumber);
                    $callback->setTransactionDate($TransactionDate);
                    $callback->setAmount($Amount);
                    $callback->setPhoneNumber($PhoneNumber);
    
                    $this->save($callback);
                    $msg = "Payment successful:".$Amount;
                    $ret = ['success', $msg];
                    // $this->sendSms->quickSend($PhoneNumber, 'online client', 'payment-'.$MpesaReceiptNumber, 'Thank you! Payment of KSh' . $Amount . ' for ' . $purpose . ' is successful. Receipt Number: '.$MpesaReceiptNumber, 'necessary', 'personal_sms_from_office', 'nomail');
                    // $this->sendSms->quickSend('0705285959', 'admin', 'payment-'.$MpesaReceiptNumber, 'Payment for ' . $purpose . ' by ' . $PhoneNumber . ' of KSh' . $Amount . ' is successful. Receipt Number: '.$MpesaReceiptNumber, 'necessary', 'personal_sms_from_office', 'nomail');
                    return $ret;
    
                }

            } else {

                $msg = $status->ResultDesc;
                $errors['error'] = $msg;
                $ret = ['failed', $errors];
                
                return $ret;
            }
        }

        // return new JsonResponse($status);

    }

    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    function getVar($hay, $needle, $level){

        // body and stkCallback
        $body = $hay['Body'];
        $stkCallback = $body['stkCallback'];

        // depending on array level provided to 3rd parameter,
        // give back the carrier which contains the value
        // given as the second parameter
        if($level == 0){
            $carrier = $body;
        }
        if($level == 1){
            $carrier = $stkCallback;
        }
        if($level == 2){
            $carrier = $stkCallback[$needle];
        }
        if($level == 3){
            $Item = $stkCallback['CallbackMetadata']['Item'];
            $columns = array_column($Item, 'Value', 'Name');
            $carrier = $columns[$needle];
        }

        return $carrier;
    }

    public function formatPhoneNumber($phone){

        //The default country code if the recipient's is unknown:
        $country_code  = '254';

        //Remove any parentheses and the numbers they contain:
        $phone = preg_replace("/\([0-9]+?\)/", "", $phone);

        //Strip spaces and non-numeric characters:
        $phone = preg_replace("/[^0-9]/", "", $phone);

        //Strip out leading zeros:
        $phone = ltrim($phone, '0');

        //Look up the country dialling code for this number:
        $pfx = $country_code;

        //Check if the number doesn't already start with the correct dialling code:
        if ( !preg_match('/^'.$pfx.'/', $phone)  ) {
            $phone = $pfx.$phone;
        }

        //return the converted number:
        return $phone;        

    }



}
