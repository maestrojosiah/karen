<?php 

namespace App\Service;

use App\Repository\UserRepository;
use App\Repository\AttachmentRepository;
use App\Repository\GroupRepository;
use App\Entity\Communication;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\Mailer;

class SendSms extends AbstractController
{

    private $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;

    }
    // function CallAPI()
    function CallAPI($method, $url, $data = false)
    {

      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => "$url",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "$method",
        CURLOPT_POSTFIELDS => "$data",
        CURLOPT_HTTPHEADER => array(
          "apikey: 7ffa77d91d05a5ba1d16ee1952f9064ca2f84925",
          "cache-control: no-cache",
          "content-type: application/x-www-form-urlencoded"
        ),
      ));
    
      $response = curl_exec($curl);
      $err = curl_error($curl);
    
      curl_close($curl);
    
      if ($err) {
        return "cURL Error #:" . $err;
      } else {
        return $response;
      }
      
    }

    function getReport($method, $url, $data = false ){

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "$url",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "$method",
        CURLOPT_POSTFIELDS => "$data",
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
          "content-type: application/x-www-form-urlencoded"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        return "cURL Error #:" . $err;
      } else {
        return $response;
      }
      
    }

    function getReports($method, $url, $data = false){
        // Delivery reports can only be retrieved one time. Once you retrieve a delivery report, you 
        // will not be able to get the same report again by using this endpoint
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "$method",
          CURLOPT_POSTFIELDS =>"$data",
          CURLOPT_HTTPHEADER => array(
            'Authorization: Basic Y29uc2VydmtjbTpFaXJ3bXItMzMtaHA=',
            'Content-Type: application/json'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;
        
    }

    function getLogs($uuid, $datefrom, $dateto){
        // Unlike delivery reports, these logs can be requested as many times as you want

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://smsportal.hostpinnacle.co.ke/SMSApi/report/status?userid=Muske2022&password=Eirwmr-33@hp&uuid=$uuid&fromdate=$datefrom&todate=$dateto&output=json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_HTTPHEADER => array(
            'x-api-key: e3a1fef81da173a25d750f8078d99f14dc3488da',
            'Cookie: SERVERID=webC1; PHPSESSID=t55mnu4u42jmc31urrv2l2l14s; SERVERNAME=s1'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return $response;

    }


    /**
     * @Route("/communication/get/message/log/{para}/{id}", name="getlogs")
     */
    public function getLogsFunc(SendSms $sendSms, Request $request, $para, $id){

        $today = date("Y-m-d");
        $dataL = "userid=Muske2022&password=Eirwmr-33@hp&uuid=$id&fromdate=$today&todate=$today&output=json";

        try {
            $reportresponse = $this->getLogs($id, $today, $today);
            // $reportresponse = "{\"response\":{\"api\":\"send\",\"action\":\"status\",\"status\":\"success\",\"msg\":\"success\",\"code\":\"200\",\"count\":4,\"report_statusList\":[{\"status\":{\"uuId\":\"1171387500624170825\",\"msgId\":\"jlZQFnKswD8lC0P\",\"mobileNo\":\"254711476249\",\"senderName\":\"MUSKE-MUSIC\",\"text\":\"Testingthemessagingsystem.Ignore\",\"msgType\":\"text\",\"length\":\"36\",\"cost\":\"1\",\"globalErrorCode\":\"0\",\"submitTime\":\"1646198756563\",\"deliveryTime\":\"1646198800131\",\"Status\":\"DELIVERED\",\"Cause\":\"Delivered\",\"ChannelName\":\"API\",\"SubmittedTime\":\"March2,20228:25AM\",\"DeliveredTime\":\"March2,20228:26AM\"}},{\"status\":{\"uuId\":\"1171387500624170825\",\"msgId\":\"j4vR94BlyvZIzDi\",\"mobileNo\":\"254705285959\",\"senderName\":\"MUSKE-MUSIC\",\"text\":\"Testingthemessagingsystem.Ignore\",\"msgType\":\"text\",\"length\":\"36\",\"cost\":\"1\",\"globalErrorCode\":\"0\",\"submitTime\":\"1646198756563\",\"deliveryTime\":\"1646198767404\",\"Status\":\"DELIVERED\",\"Cause\":\"Delivered\",\"ChannelName\":\"API\",\"SubmittedTime\":\"March2,20228:25AM\",\"DeliveredTime\":\"March2,20228:26AM\"}},{\"status\":{\"uuId\":\"1171387500624170825\",\"msgId\":\"gpCsviHLlhnn8oO\",\"mobileNo\":\"254736600033\",\"senderName\":\"MUSKE-MUSIC\",\"text\":\"Testingthemessagingsystem.Ignore\",\"msgType\":\"text\",\"length\":\"36\",\"cost\":\"1\",\"globalErrorCode\":\"11\",\"submitTime\":\"1646198756563\",\"deliveryTime\":\"0\",\"Status\":\"SUBMITTED\",\"Cause\":\"SubmittedtoOperator\",\"ChannelName\":\"API\",\"SubmittedTime\":\"March2,20228:25AM\",\"DeliveredTime\":\"InProcess\"}},{\"status\":{\"uuId\":\"1171387500624170825\",\"msgId\":\"y3LcaiJFxG4G0zK\",\"mobileNo\":\"254711832933\",\"senderName\":\"MUSKE-MUSIC\",\"text\":\"Testingthemessagingsystem.Ignore\",\"msgType\":\"text\",\"length\":\"36\",\"cost\":\"1\",\"globalErrorCode\":\"0\",\"submitTime\":\"1646198756563\",\"deliveryTime\":\"1646198800442\",\"Status\":\"DELIVERED\",\"Cause\":\"Delivered\",\"ChannelName\":\"API\",\"SubmittedTime\":\"March2,20228:25AM\",\"DeliveredTime\":\"March2,20228:26AM\"}}]}}";
            $reportres[] = $reportresponse;
            $reportres = $this->jsonToAssocArray($reportresponse);
            // $deliveredStatus = " SMS_" . $this->getResponsesVar($reportres, 'groupName', 3, 'status');
            // $error = " SMS_ERR_" . $this->getResponsesVar($reportres, 'groupName', 3, 'error');               
        } catch (HttpException $ex) {
            echo $ex;
        }

        $table = $this->arrayToTable($reportres['response']['report_statusList']);
        return new JsonResponse($table);

    }

    public function arrayToTable($data)
    {
        $rows = array();
        foreach ($data as $row) {
            $cells = array();
            foreach ($row as $key => $cell) {

                $cells[] = "<tr><td>" . $cell['mobileNo'] . "</td>";
                $cells[] = "<td>" . $cell['Status'] . "</td>";
                $cells[] = "<td>" . $cell['Cause'] . "</td></tr>";
                // $rows[$key] = $cell;
            }
            $rows[] = implode('', $cells);
        }
        return "<table class='table'>" . implode('', $rows) . "</table>";
        // return $rows;
    }

    public function writeToFile($data, $filename){
        $proj_dir = $this->getParameter('proj_dir');
        $file = $proj_dir.'/'. $filename . '.txt';

        if (!file_exists($file)) {
            touch($file);
        }
        // Open the file to get existing content
        $current = file_get_contents($file);
        // Append a new person to the file
        $current .= "\r\n".$data;
        // Write the contents back to the file
        return file_put_contents($file, $current);

    }
    public function jsonToAssocArray($jsonstring){
        $array = [];
        $assocArray = json_decode($jsonstring, true);
        foreach ($assocArray as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }

    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    /**
     * @Route("/texting/send/sms/{to}/{msg}", name="texting")
     */
    public function sendSMSLink(SendSms $sendSms, Request $request, $to, $msg)
    {
    
        if ($to !== "" && $msg !== "") {

            $phone_to = $this->fullNumber($request->request->get('phone_to'));
            $res = [];
            $data = "{\n \"from\":\"ModuleZilla\",\n \"to\":\"$phone_to\",\n \"text\":\"$msg\"\n}";

            try {
                $response = $this->CallAPI('POST', 'http://54.247.191.102/restapi/sms/1/text/single', $data );
                $res = $response;
            } catch (HttpException $ex) {
                echo $ex;
            }

            return new JsonResponse($msg);

        }
    
    }

    public function fullNumber($num){
    	$number = "";
        if(strlen($num) == 10){
            $number = preg_replace('/^0/', '+254', $num);
        } elseif(strlen($num) == 13) {
            $number = $num; 
        } else {
            $number = $num;
        }
        return $number;
    }

    /**
     * @Route("/send/otp/to/number", name="sendotp")
     */
    public function sendOtp(SendSms $sendSms, Request $request)
    {

        $phonenumber = $request->request->get('phonenumber');
        $phone_to = "";
        $msg = "test";
        if ($phonenumber !== "") {
            $phone_to = $this->fullNumber($phonenumber);
            $res = [];
            $data = "{\n \"from\":\"MUSKE_MUSIC\",\n \"to\":\"$phone_to\",\n \"text\":\"$msg\"\n}";

            try {
                $response = $this->CallAPI('POST', 'http://54.247.191.102/restapi/sms/1/text/single', $data );
                $res = $response;
            } catch (HttpException $ex) {
                echo $ex;
            }

            return new JsonResponse($res);

        }
    

        return new JsonResponse("res");
    }

    public function callBack() {
               
        if($json = json_decode(file_get_contents("php://input"), true)) {
            $CheckoutRequestID = $this->getVar($json, 'CheckoutRequestID', 2);
            $entityManager = $this->getDoctrine()->getManager();
            $callback = new Callback();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $entityManager->persist($callback);
            $entityManager->flush();        
            // $this->followUp($CheckoutRequestID);
        } else {
            $json = $_POST;
            $CheckoutRequestID = $this->getVar($json, 'CheckoutRequestID', 2);
            $entityManager = $this->getDoctrine()->getManager();
            $callback = new Callback();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $entityManager->persist($callback);
            $entityManager->flush();        
            // $this->followUp($CheckoutRequestID);
        }
    }

    function getBulkId($hay){
        if(isset($hay['bulkId'])){
            $bulkId = $hay['bulkId'];
        } else {
            $bulkId = $this->getMessagesVar($hay, 'messageId', 2);
        }
        
        return $bulkId;
    }
    
    function getMessagesVar($hay, $needle, $further = "", $furthermore = "", $furthermost = "", $evenfurther = ""){

        $body = $hay["$needle"];
        $carrier = $body;

        // depending on array level provided to 3rd parameter,
        // give back the carrier which contains the value
        // given as the second parameter
        if($further != ""){
            $carrier = $body["$further"];
        }
        if($furthermore != ""){
            $carrier = $body["$further"]["$furthermore"];
        }
        if($furthermost != ""){
            $carrier = $body["$further"]["$furthermore"]["$furthermost"];
        }
        if($evenfurther != ""){
            $carrier = $body["$further"]["$furthermore"]["$furthermost"]["$evenfurther"];
        }

        return $carrier;
    }

    function getResponsesVar($hay, $needle, $further = "", $furthermore = "", $furthermost = "", $evenfurther = ""){

        $body = $hay["$needle"];
        $carrier = $body;

        // depending on array level provided to 3rd parameter,
        // give back the carrier which contains the value
        // given as the second parameter
        if($further != ""){
            $carrier = $body["$further"];
        }
        if($furthermore != ""){
            $carrier = $body["$further"]["$furthermore"];
        }
        if($furthermost != ""){
            $carrier = $body["$further"]["$furthermore"]["$furthermost"];
        }
        if($evenfurther != ""){
            $carrier = $body["$further"]["$furthermore"]["$furthermost"]["$evenfurther"];
        }


        return $carrier;
    }


    function splitPhoneNumbers($str){
        $trimmed = preg_replace("/\s+/", "", $str);
        $arrOfNum = explode(',', $trimmed);
        $len = count($arrOfNum);
        $i = 0;
        $numStr = "[";
        foreach ($arrOfNum as $num ) {
            $fullNum = $this->fullNumber($num);
            $numStr .= '"'.$fullNum.'"';
            if ($i != $len - 1) {
                $numStr .= ',';
            }       
            $i++; 
        }
        $numStr .= "]";
        return $numStr;
    }

    public function quickSend($phone, $name, $subject, $msg, $category, $messagetype, $email)
    {
        $sent_to = $phone;
        $name = $name;
        $phone_to = $phone;
        $subject = $subject;
        $msg = $msg;
        $sent_to_contact = "";
        $sentStatus = "";
        $deliveredStatus = "N/A";
        $error = "";
        $type = "";
        $res = [];
        $reportres = [];
        $messageId = "";

        if($email !== 'nomail'){

            $user = $this->userRepo->findOneByEmail($email);
            $usersettings = $user->getUserSettings();
    
            $smsSend = true;

        } else {
            $smsSend = true;
        }

        if($smsSend == true ){
            $em = $this->getDoctrine()->getManager();
        
            if(null == $this->getUser()){
              $admin = $this->userRepo->findOneById(1);
            } else {
              $admin = $this->getUser();
            }
    
            // instantiate communication
            $communication = new Communication();
            // time now
            $time_now = new \Datetime();
            // set sent at
            $communication->setSentAt($time_now);
    
            $resp = "";
            $data = "";
            $contacts = $this->splitPhoneNumbers($phone_to);
    
            if(null != $phone_to){
                $sent_to_contact .= " " . $phone_to . " ";
                $type .= "SMS";
    
                if($sent_to == ""){
                    $sent_to = $phone_to;
                }
                if($subject == ""){
                    $subject = substr($msg, 0, 20);
                }
        
                if ($phone_to !== "" && $msg !== "") {
                    
                    $msg = htmlspecialchars_decode(trim(strip_tags($msg)), ENT_QUOTES);
                    // $msg = str_replace(PHP_EOL, "\\r\\n", $msg);
    
                    $data = "userid=Muske2022&password=Eirwmr-33@hp&sendMethod=quick&mobile=$contacts&msg=$msg&senderid=MUSKE-MUSIC&msgType=text&duplicatecheck=true&output=json";
        
                    try {
                        $response = $this->CallAPI('POST', 'https://smsportal.hostpinnacle.co.ke/SMSApi/send', $data );
                        // $response = "{\"status\":\"success\",\"mobile\":\"254705285959\",\"invalidMobile\":\"\",\"transactionId\":\"6147944372327631476\",\"statusCode\":\"200\",\"reason\":\"success\",\"msgId\":\"\"}";
                                    
                        $res[] = $response;
                        $res = $this->jsonToAssocArray($response);
                        $status = $this->getMessagesVar($res, 'status', 0);
                        $messageId = $this->getMessagesVar($res, 'transactionId', 0);
                        $sentStatus .= " SMS_SENDING_$status";
                        $resp .= "SMS sent";
                    } catch (HttpException $ex) {
                        $sentStatus .= ' SMS_NOT_SENT';
                        echo $ex;
                    }
    
                } 
        
            } 
            
            $trimmed = preg_replace("/\s+/", "", $phone_to);
            $arrOfNum = explode(',', $trimmed);
            $numOfNumbers = count($arrOfNum);
            $deliveredStatus = "";
            if($numOfNumbers > 1){
                $deliveredStatus = 'MULTIPLE';
            } else {
                $today = date("Y-m-d");
                $dataR = "userid=Muske2022&password=Eirwmr-33@hp&uuid=$messageId&fromdate=$today&todate=$today&output=json";
                try {
                    $reportresponse = $this->getReport('POST', 'https://smsportal.hostpinnacle.co.ke/SMSApi/report/status', $dataR );
                    // $reportresponse = "{\"response\": {\"api\": \"send\",\"action\": \"status\",\"status\": \"success\",\"msg\": \"success\",\"code\": \"200\",\"count\": 1,\"report_statusList\": [{\"status\": {\"uuId\": \"8359251506264886974\",\"msgId\": \"QudUrwhMuIGkbs2\",\"mobileNo\": \"919999999999\",\"senderName\": \"CBISTS\",\"text\": \"Hello world\",\"msgType\": \"text\",\"length\": \"11\",\"cost\": \"1\",\"Status\": \"FAILED\",\"Cause\": \"Unknown User\",\"Channel Name\": \"API\",\"Submitted Time\": \"July 13, 2019 03:44:48\",\"Delivered Time\": \"July 13, 2019 03:44:49\"}}]}}";
                    $reportres[] = $reportresponse;
                    $reportres = $this->jsonToAssocArray($reportresponse);
                    $deliveredStatus = " SMS_" . $this->getResponsesVar($reportres, 'response', 'status');
                    // $cause = $this->getResponsesVar($reportres, 'response', 'report_statusList', "0", 'status', 'Cause');
                    // $error = " SMS_ERR_" . $this->getResponsesVar($reportres, 'groupName', 3, 'error');               
                } catch (HttpException $ex) {
                    echo $ex;
                }
            }
    
            $communication->setSentTo($name);
            $communication->setSendToContact($sent_to_contact);
            $communication->setMessage($msg);
            $communication->setType($type);
            $communication->setSubject($subject);
            $communication->setSentStatus($sentStatus);
            $communication->setDeliveredStatus($deliveredStatus);
            $communication->setMessageId($messageId);
            $communication->setOpenedStatus('N/A');
            $communication->setDeliveredAt(new \Datetime('1970/01/01'));
            $communication->setAdmin($admin);
    
            $this->save($communication);
    
            return true;

        } else {

            if($messagetype == 'todays_lessons_sms' || $messagetype == 'todays_lessons_email'){
                // do nothing
            } else {
                $m = substr($msg, 0, 50);
                $data = "userid=Muske2022&password=Eirwmr-33@hp&sendMethod=quick&mobile=254705285959&msg=Couldnt+send+message+to+$phone+because+of+their+notification+settings-+$m&senderid=MUSKE-MUSIC&msgType=text&duplicatecheck=true&output=json";
        
                try {
                    $response = $this->CallAPI('POST', 'https://smsportal.hostpinnacle.co.ke/SMSApi/send', $data );
                } catch (HttpException $ex) {
                    echo $ex;
                }
            }
            return false;

        }
    }


}
