<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use App\Entity\User;
use Knp\Snappy\Pdf;
use Twig\Environment;

class Mailer 
{
    private $mailer;
    private $pdf;
    private $twig;

    public function __construct(MailerInterface $mailer, Pdf $pdf, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->pdf = $pdf;
        $this->twig = $twig;

    }

    public function sendEmailMessage($data, $sendto, $subject, $template, $category, $emailtype)
    {
            $email = (new TemplatedEmail())
            ->from(new Address('info@crystalshea.co.ke', 'Crystal Shea'))
            ->to(new Address($sendto))
            ->subject($subject)
        
            // path of the Twig template to render
            ->htmlTemplate('emails/'.$template)
        
            // pass variables (name => value) to the template
            ->context($data);
    
            $this->mailer->send($email);
            return true;
    

    }


    public function sendEmailWithAttachment($data, $sendto, $subject, $html, $template, $docname)
    {
        // $html = $this->twig->render('emails/author-weekly-report-pdf.html.twig', [
        //     'articles' => $articles,
        // ]);
        $pdf = $this->pdf->getOutputFromHtml($html);

        $email = (new TemplatedEmail())
        ->from(new Address('info@crystalshea.co.ke', 'Crystal Shea'))
        ->to(new Address($sendto))
        ->subject($subject)
        ->htmlTemplate('emails/'.$template)
        ->context($data)
        ->attach($pdf, sprintf($docname.'-%s.pdf', date('Y-m-d')));

        $this->mailer->send($email);
        return true;

    }

}