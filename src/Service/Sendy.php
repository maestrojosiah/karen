<?php 

namespace App\Service;

use App\Entity\Communication;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Sendy extends AbstractController
{

    function CallAPI($method, $url, $data = false)
    {

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "$method",
          CURLOPT_POSTFIELDS =>"$data",
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Content-Type: application/json",
            'Authorization: Bearer MTA.GUWjYR7kpJaUOAoq0ev0snDyxvhp_lt5pdebyhGB02mheGMZb1k2_lFiLw15',
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);

        return $response;

    }

    public function jsonToAssocArray($jsonstring){
      $array = [];
      $assocArray = json_decode($jsonstring, true);
      foreach ($assocArray as $key => $value) {
          $array[$key] = $value;
      }
      return $array;
    }

    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        if($entityManager->flush()){
            return true;
        } else {
            return false;
        }
    }

    public function fullNumber($num){
        $number = "";
        if(strlen($num) == 10){
            $number = preg_replace('/^0/', '+254', $num);
        } elseif(strlen($num) == 13) {
            $number = $num; 
        } else {
            $number = $num;
        }
        return $number;
    }

    function getBulkId($hay){
        if(isset($hay['bulkId'])){
            $bulkId = $hay['bulkId'];
        } else {
            $bulkId = $this->getMessagesVar($hay, 'messageId', 2);
        }
        
        return $bulkId;
    }

    function getMessagesVar($hay, $needle, $level){

        // body and callback
        $body = $hay['messages'];
        $callback = $body[0];

        // depending on array level provided to 3rd parameter,
        // give back the carrier which contains the value
        // given as the second parameter
        if($level == 0){
            $carrier = $body;
        }
        if($level == 1){
            $carrier = $callback;
        }
        if($level == 2){
            $carrier = $callback[$needle];
        }
        if($level == 3){
            $carrier = $callback['status'][$needle];
        }

        return $carrier;
    }

    function getResponsesVar($hay, $needle, $level, $category=""){

        // body and callback
        $body = $hay['results'];
        $callback = $body[0];

        // depending on array level provided to 3rd parameter,
        // give back the carrier which contains the value
        // given as the second parameter
        if($level == 0){
            $carrier = $body;
        }
        if($level == 1){
            $carrier = $callback;
        }
        if($level == 2){
            $carrier = $callback[$needle];
        }
        if($level == 3){
            $carrier = $callback[$category][$needle];
        }

        return $carrier;
    }

    function splitPhoneNumbers($str){
        $trimmed = preg_replace("/\s+/", "", $str);
        $arrOfNum = explode(',', $trimmed);
        $len = count($arrOfNum);
        $i = 0;
        $numStr = "[";
        foreach ($arrOfNum as $num ) {
            $fullNum = $this->fullNumber($num);
            $numStr .= '"'.$fullNum.'"';
            if ($i != $len - 1) {
                $numStr .= ',';
            }       
            $i++; 
        }
        $numStr .= "]";
        return $numStr;
    }


}