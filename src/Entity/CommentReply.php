<?php

namespace App\Entity;

use App\Repository\CommentReplyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentReplyRepository::class)
 */
class CommentReply
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $r_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $r_email;

    /**
     * @ORM\ManyToOne(targetEntity=Comment::class, inversedBy="commentReplies")
     */
    private $comment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getRName(): ?string
    {
        return $this->r_name;
    }

    public function setRName(string $r_name): self
    {
        $this->r_name = $r_name;

        return $this;
    }

    public function getREmail(): ?string
    {
        return $this->r_email;
    }

    public function setREmail(string $r_email): self
    {
        $this->r_email = $r_email;

        return $this;
    }

    public function getComment(): ?Comment
    {
        return $this->comment;
    }

    public function setComment(?Comment $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
