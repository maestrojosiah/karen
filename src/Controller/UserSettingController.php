<?php

namespace App\Controller;

use App\Entity\UserSetting;
use App\Form\UserSettingType;
use App\Repository\SettingRepository;
use App\Repository\UserSettingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user/setting")
 */
class UserSettingController extends AbstractController
{
    /**
     * @Route("/", name="user_setting_index", methods={"GET"})
     */
    public function index(UserSettingRepository $userSettingRepository): Response
    {
        return $this->render('user_setting/index.html.twig', [
            'user_settings' => $userSettingRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{description}", name="user_setting_new", methods={"GET","POST"})
     */
    public function new(Request $request, SettingRepository $settingRepo, UserSettingRepository $uSRepo, $description = ""): Response
    {
        if($description != ""){
            $setting = $settingRepo->findOneByDescription($description);
            if(null !== $uSRepo->findOneBySetting($setting)){
                $val = $uSRepo->findOneBySetting($setting)->getValue();
            } else {
                $val = null;
            }
        } else {
            $setting = null;
            $val = "";
        }
        $userSetting = new UserSetting();
        $form = $this->createForm(UserSettingType::class, $userSetting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userSetting->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userSetting);
            $entityManager->flush();

            return $this->redirectToRoute('user_setting_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user_setting/new.html.twig', [
            'user_setting' => $userSetting,
            'setting' => $setting,
            'val' => $val,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="user_setting_show", methods={"GET"})
     */
    public function show(UserSetting $userSetting): Response
    {
        return $this->render('user_setting/show.html.twig', [
            'user_setting' => $userSetting,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_setting_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UserSetting $userSetting): Response
    {
        $form = $this->createForm(UserSettingType::class, $userSetting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_setting_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user_setting/edit.html.twig', [
            'user_setting' => $userSetting,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="user_setting_delete", methods={"POST"})
     */
    public function delete(Request $request, UserSetting $userSetting): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userSetting->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userSetting);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_setting_index', [], Response::HTTP_SEE_OTHER);
    }
}
