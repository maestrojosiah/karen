<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Manager\CartManager;
use App\Form\CartType;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Security\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use App\Form\RegistrationFormType;
use App\Repository\ProductRepository;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\AuthenticatorInterface;
use App\Security\SecurityAuthenticator;
use App\Security\AlternativeSecurityAuthenticator;
use App\Service\Sendy;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\HttpFoundation\JsonResponse;

class CartController extends AbstractController
{



    private $emailVerifier;
    private $userRepo;

    public function __construct(EmailVerifier $emailVerifier, UserRepository $userRepo )
    {
        $this->emailVerifier = $emailVerifier;
        $this->userRepo = $userRepo;
    }
    /**
     * @Route("/cart", name="cart")
     */
    public function index(CartManager $cartManager, Request $request): Response
    {
        $cart = $cartManager->getCurrentCart();
        $form = $this->createForm(CartType::class, $cart);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->addFlash(
                'success',
                'Changes made successfully. ',
            );
    
            $cart->setUpdatedAt(new \DateTime());
            $cartManager->save($cart);

            return $this->redirectToRoute('cart');
        } 

        return $this->render('cart/index.html.twig', [
            'cart' => $cart,
            'form' => $form->createView()
        ]);
    }

    // /**
    //  * @Route("/checkout_final", name="checkout")
    //  */
    // public function checkout(AuthenticationUtils $authenticationUtils, CartManager $cartManager, Request $request): Response
    // {
    //     $cart = $cartManager->getCurrentCart();
    //     $form = $this->createForm(CartType::class, $cart);

    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $cart->setUpdatedAt(new \DateTime());
    //         $cartManager->save($cart);

    //         return $this->redirectToRoute('cart');
    //     } 

    //     // get the login error if there is one
    //     $error = $authenticationUtils->getLastAuthenticationError();
    //     // last username entered by the user
    //     $lastUsername = $authenticationUtils->getLastUsername();

    //     return $this->render('cart/checkout_acc.html.twig', [
    //         'last_username' => $lastUsername, 
    //         'error' => $error,
    //         'cart' => $cart,
    //         'form' => $form->createView()
    //     ]);
    // }


    /**
     * @Route("/checkout/account/check", name="checkout_account")
     */
    public function checkoutAccCheck(UserRepository $userRepo, AuthenticationUtils $authenticationUtils, UserPasswordHasherInterface $passwordEncoder, CartManager $cartManager, Request $request): Response
    {

        // get the current cart
        $cart = $cartManager->getCurrentCart();
        // if there is an item or more,
        if(count($cart->getItems()) > 0){

            // see if there is a logged in user
            $user = $this->getUser();
            // if there is,
            if(null != $user){
                // redirect to address tab
                return $this->redirectToRoute('checkout_address');
            // otherwise load the page for user login / register
            } else {

                // get the login error if there is one
                $error = $authenticationUtils->getLastAuthenticationError();
                // last username entered by the user
                $lastUsername = $authenticationUtils->getLastUsername();

                return $this->render('cart/checkout_acc.html.twig', [
                    'last_username' => $lastUsername, 
                    'error' => $error,
                ]);

            }
        // if there is nothing in the cart
        } else {
            // redirect to the home page
            return $this->redirectToRoute('default');

        }
    }
    /**
     * @Route("/checkout/save/user", name="checkout_save_new_user")
     */
    public function saveUserFromCheckout(UserRepository $userRepo, AlternativeSecurityAuthenticator $auth, EntityManagerInterface $em, UserAuthenticatorInterface $authenticator, AuthenticationUtils $authenticationUtils, UserPasswordHasherInterface $passwordEncoder, CartManager $cartManager, Request $request): Response
    {

        $user = new User();

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            // encode the plain password
            // get the form data
            $fname = $_POST['fname'];
            $lname = $_POST['lname'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            // die();
            
            // the current cart
            $cart = $cartManager->getCurrentCart();

            // check if the email is already in the database
            $userExists = $userRepo->findOneByEmail($email);

            print_r($cart->getId());
            echo "<pre>";

            // if the user exists,
            if($userExists){

                // see if there is an order from the same user
                $existingItem = $this->getSqlResult($em, "SELECT * FROM `order` WHERE user_id = " . $userExists->getId() . " AND status = 'cart'");

                // if there is an existing order
                if(!empty($existingItem)) {
                    $addToExistingOrder = $this->getSqlResult($em, "UPDATE `order_item` SET order_ref_id = " . $existingItem[0]['id'] . " WHERE order_ref_id = " . $cart->getId());  
                    // print_r('not empty cart items: ');
                    // print_r(count($cart->getItems()));
                    // die();
                    $orderItemsWithDuplicates = $this->getSqlResult($em, "SELECT order_ref_id, product_id
                    FROM order_item
                    GROUP BY order_ref_id, product_id
                    HAVING COUNT(*) > 1");

                    if (count($orderItemsWithDuplicates) > 0) {
                        // the query has results

                        foreach ($orderItemsWithDuplicates as $key => $orda) {

                            # code...
                            $prod_id = $orda['product_id'];
                            $o_id = $this->getSqlResult($em, "SELECT id
                            FROM order_item
                            WHERE order_ref_id = " . $existingItem[0]['id'] . " AND product_id = " . $prod_id . "
                            ORDER BY id ASC
                            LIMIT 1");
        
                            $qtySum = $this->getSqlResult($em, "SELECT SUM(quantity) AS t
                            FROM order_item
                            WHERE order_ref_id = " . $existingItem[0]['id'] . " AND product_id = " . $prod_id );
                            
                            $updateQuantityForFirstEntry = $this->getSqlResult($em, "UPDATE order_item
                            SET quantity = " . $qtySum[0]['t'] . "
                            WHERE id = " . $o_id[0]['id'] );
        
                            $deleteTheRest = $this->getSqlResult($em, "DELETE FROM order_item
                            WHERE order_ref_id = " . $existingItem[0]['id'] . " AND product_id = " . $prod_id . " AND id != " . $o_id[0]['id']);
                            
                        }
    
                        // process the rows
                    } else {
                        // the query has no results
                    }
  
                //     // $delItems   = $this->getSqlResult($em, "DELETE FROM `order_item` WHERE order_ref_id = " . $cart->getId());
                //     // $delOrder   = $this->getSqlResult($em, "DELETE FROM `order` WHERE user_id = " . $userExists->getId());
                //     // add the items to the existing order.
                      
                // // if there is no existing order from the same user
                } else {

                    // assign the user to the current cart
                    $cart->setUser($userExists);
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($cart);
                    $entityManager->flush();
                    // print_r('empty');
                    // print_r($existingItem);
                    // die();

                }
                // authenticate user and login
                return $authenticator->authenticateUser(
                    $userExists, 
                    $auth, 
                    $request
                ); 
            // if the user does not exist
            } else {
                // hash the provided password
                $user->setPassword(
                    $passwordEncoder->hashPassword(
                        $user,
                        $password
                    )
                );
                // create the user
                $user->setUsertype('normal');
                $user->setFname($fname);
                $user->setLname($lname);
                $user->setPhone($phone);
                $user->setEmail($email);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                // generate a signed url and email it to the user
                $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                    (new TemplatedEmail())
                        ->from(new Address('info@crystalshea.co.ke', 'Crystal Shea'))
                        ->to($user->getEmail())
                        ->subject('Please Confirm your Email')
                        ->htmlTemplate('registration/confirmation_email.html.twig')
                );
                // do anything else you need here, like send an email

                // if there are cart items, assign them to the new user
                if(count($cart->getItems()) > 0){
                    $cart->setUser($user);

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($cart);
                    $entityManager->flush();
                    // authenticate and login the user
                    return $authenticator->authenticateUser(
                        $user, 
                        $auth, 
                        $request
                    ); 
                } else {

                    return $authenticator->authenticateUser(
                        $user, 
                        $auth, 
                        $request
                    ); 
                    
                }

    
            }
        }
    }

    public function getSqlResult($em, $sql)
    {   
    
        $stmt = $em->getConnection()->prepare($sql);
        $result = $stmt->executeQuery()->fetchAllAssociative();
        return $result;

    }

    /**
     * @Route("/checkout/save/shipping/fee", name="checkout_save_fee")
     */
    public function checkoutSaveFee(EntityManagerInterface $em, Request $request)
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            // encode the plain password
            // echo "<pre>";
            // get the form data

            // print_r($_POST);
            // die();

            $order_id = $_POST['order_id'];
            $shippingFee = $_POST['shippingFee'];
            $deliveryOptions = $_POST['deliveryOptions'];
            $fulfilmentFee = $_POST['fulfilmentFee'];

            $order = $this->getSqlResult($em, "SELECT * FROM `order` WHERE id = " . $order_id);

            $updateOrder = $this->getSqlResult($em, "UPDATE `order` SET shippingfee = '" . $shippingFee . "' WHERE id = " . $order[0]['id']);    
            
            return $this->redirectToRoute('checkout_payment', ['order_id' => $order_id]);

        }
    }


    /**
     * @Route("/checkout/address/details", name="checkout_address")
     */
    public function checkoutAddress(CartManager $cartManager, Request $request): Response
    {
        $user = $this->getUser();
        if(null == $user){
            // redirect to address tab
            return $this->redirectToRoute('app_login');
        // otherwise load the page for user login / register
        } else {

            return $this->render('cart/checkout_address.html.twig', [
                'cart' => $cartManager->getCurrentCart(),
            ]);

        }
    }

    /**
     * @Route("/checkout/delivery/information", name="checkout_delivery")
     */
    public function checkOutDelivery(CartManager $cartManager, Request $request): Response
    {
        $user = $this->getUser();
        $cart = $cartManager->getCurrentCart();
        $cart->setShippingfee(0);
        $this->save($cart);
        if(null == $user){
            // redirect to address tab
            return $this->redirectToRoute('app_login');
        // otherwise load the page for user login / register
        } else {

            return $this->render('cart/checkout_delivery.html.twig', [
                'cart' => $cart,
            ]);
        }
    }

    /**
     * @Route("/checkout/payment/information/{order_id}", name="checkout_payment")
     */
    public function checkoutPayment(CartManager $cartManager, EntityManagerInterface $em, $order_id): Response
    {

        $user = $this->getUser();
        $order = $this->getSqlResult($em, "SELECT * FROM `order` WHERE id = " . $order_id);

        // $updateOrder = $this->getSqlResult($em, "UPDATE `order` SET shippingfee = '" . $shippingFee . "' WHERE id = " . $order[0]['id']);    
        // $updateOrder = $this->getSqlResult($em, "UPDATE `order` SET shippingfee = 0 WHERE id = " . $order[0]['id']);    
        if(null == $user){
            // redirect to address tab
            return $this->redirectToRoute('app_login');
        // otherwise load the page for user login / register
        } else {
            return $this->render('cart/checkout_payment.html.twig', [
                'cart' => $cartManager->getCurrentCart(),
                'shippingFee' => $order[0]['shippingfee'],
                'payFor' => "Shopping",
                'order_id' => $order[0]['id'],
            ]);
        }
    }

    /** 
     * @Route("/checkout/check/delivery/fee", name="checkout_delivery_check")
     */
    public function checkoutDeliveryCheck(Request $request, ProductRepository $productRepo, Sendy $sendy, EntityManagerInterface $em){

        $order_id = $request->request->get('order_id');
        // echo "<pre>";
        $order = $this->getSqlResult($em, "SELECT * FROM `order` WHERE id = " . $order_id);
        $orderItems = $this->getSqlResult($em, "SELECT * FROM `order_item` WHERE order_ref_id = " . $order[0]['id']);
        $inputs = [];
        foreach ($orderItems as $oi ) {
            $product_id = $oi['product_id'];
            $product = $productRepo->findOneById($product_id);
            $prodSize = preg_replace('~\D~', '', $product->getSize());
            $inputs[$product->getName() . ' ' . $prodSize] = $oi['quantity'];
        }

        // $inputs = ['Shea Hot 120' => '2', 'Afro Gold 250' => '3', 'Whip Lasting 400' => '1', 'Afro Gold 60' => '2'];

        $data = "{ \r\n \"api_username\": \"B-PGA-8166\", \r\n \"api_key\": \"MTA.GUWjYR7kpJaUOAoq0ev0snDyxvhp_lt5pdebyhGB02mheGMZb1k2_lFiLw15\", \r\n \"stock_levels\": \"available\", \r\n \"limit\": \"1000\", \r\n \"page\": \"0\" \r\n }";

        try {
            $response = $sendy->CallAPI('GET', 'https://fulfillment-api.sendyit.com/v1/products', $data );
        } catch (HttpException $ex) {
            echo $ex;
        }
        // get all the products and variants
            
        // make a php array of products key=>array : id->name
        $array = $sendy->jsonToAssocArray($response);
        // print_r($array);
        // die();

        
        $message = $array['message'];
        $products = $array['data'];
        $product_names = [];
        $product_entity = [];
        $words = [];
        $prods = [];
        $ps = [];

        foreach($products['products'] as $product){

            $product_id = $product['product_id'];
            $product_name = $product['product_name'];
            $product_variants = $product['product_variants'];
            // echo "<br/>";

            // if it has variants, add to product name array
            if(count($product_variants) > 0){
                $variant_names = [];
                foreach($product_variants as $pv){
                    $pv_id = $pv['product_variant_id'];
                    $product_entity[$pv_id] = ['product_id' => $product_id, 'product_variant_id' => $pv_id, 'quantity' => $pv['product_variant_stock_levels']['available'], 'currency' => 'KES', 'unit_price' => $pv['product_variant_unit_price']];
                    $product_names[$pv_id] = $pv['product_variant_description'] .'_'. $pv['product_variant_quantity'] .'_'. $pv['product_variant_quantity_type'] .'_'. $pv['product_variant_unit_price'] .'_'. $pv['product_variant_stock_levels']['available'].'_'.$product_id;
                    $words[$pv_id] = $pv['product_variant_description'].'_'.$pv['product_variant_quantity'];
                }
            }
        }
        // print_r(count($inputs));
        foreach ($inputs as $input => $qty) {

            $searchFor = $input;
            $s = strtoupper($searchFor);
            $array_of_words = explode(" ", $s);
            $closest = "";
    
            foreach($product_names as $id => $p){
                $productName = strtoupper($p);
                if($this->str_contains_multiple_words($productName, $array_of_words)){
                    $closest = $productName . "_" . $id;
                    $pvId = trim(explode("_", $closest)[6]);
                } 

                // else {
                //     echo $productName . " does not have " . implode(",", $array_of_words) . "<br/>";
                // }
            }
            
            // print_r($product_entity[$pvId]);
            $entity = $product_entity[$pvId];
            $entity['quantity'] = $qty;
            // print_r($entity);
            $prods[] = $entity;
            $ps[] = $product_names[$pvId];
    
                
        }

        // print_r($product_entity[$pvId]);
        $array_for_json = ['products' => $prods ];

        $json_string_for_request = json_encode($array_for_json);

        try {
            $responseSP = $sendy->CallAPI('POST', 'https://fulfillment-api.sendyit.com/v1/orders/pricing', $json_string_for_request );
        } catch (HttpException $ex) {
            echo $ex;
        }

        $result = $sendy->jsonToAssocArray($responseSP);
        $costOfGoods = $result['data']['costOfGoods'];
        $fulfilmentFee = $result['data']['fulfilmentFee'];

        $fulfilmentData = [];
        $fulfilmentData['costOfGoods'] = $costOfGoods;
        $fulfilmentData['fulfilmentFee'] = $fulfilmentFee;

        return new JsonResponse($fulfilmentData);

    }

    public function str_contains_multiple_words($string, $array_of_words)
    {
       if (!$array_of_words) return false;
       
       $i = 0;
       
       foreach ($array_of_words as $words)
       {
            if (strpos($string, $words) !== FALSE) $i++;
       }
       
       return ($i == count($array_of_words)) ? true : false;
    }


    /**
     * @Route("/checkout/user/more/details", name="checkout_add_info")
     */
    public function checkoutSaveAddInfo(CartManager $cartManager, EntityManagerInterface $em, Request $request): Response
    {
        echo "<pre>";
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            
            $house = $_POST['house'];
            $apartment = $_POST['apartment'];
            $town = $_POST['town'];
            $order_note = $_POST['order_note'];
            $order_id = $_POST['order_id'];

            $user = $this->getUser();
            $user->setHouse($house);
            $user->setApartment($apartment);
            $user->setTown($town);

            $this->save($user);

            if(!empty($order_note)){
                $order = $this->getSqlResult($em, "SELECT * FROM `order` WHERE id = " . $order_id);
                $updateOrder = $this->getSqlResult($em, "UPDATE `order` SET ordernotes = '" . $order_note . "' WHERE id = " . $order[0]['id']);    
            } 

            return $this->redirectToRoute('checkout_delivery');

            print_r($updateOrder);
            die();
        }
            
        return $this->render('cart/checkout_add_info.html.twig', [
            'cart' => $cartManager->getCurrentCart(),
        ]);
    }
    
    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }
    
    /**
     * @Route("/check/if/email/exists", name="check_email")
     */
    public function checkIfEmailExists(Request $request){

        $emailAddress = $request->request->get('emailAddress');
        $data = [];
        $user = $this->userRepo->findFirstByEmail($emailAddress);
        if($user){
            $usertype = $user->getUsertype();
            $fname = $user->getFname();
            $lname = $user->getLname();
            $phone = $user->getPhone();
            $data['fname'] = $fname;
            $data['lname'] = $lname;
            $data['phone'] = $phone;
            $data['id'] = $user->getId();
        } else {
            $data['phone'] = '';
        }

        return new JsonResponse($data);

    }

    /**
     * @Route("/check/if/user/details/exist", name="check_details_exist")
     */
    public function checkIfDetailsExist(Request $request){

        $user_id = $request->request->get('user_id');
        $data = [];
        $user = $this->userRepo->findOneById($user_id);
        if($user){

            $house = $user->getHouse();
            $apartment = $user->getApartment();
            $town = $user->getTown();

            $data['house'] = $house;
            $data['apartment'] = $apartment;
            $data['town'] = $town;

        } else {
            $data['house'] = '';
        }

        return new JsonResponse($data);

    }

    /**
     * @Route("/checkout/update/phone/number", name="check_update_phone_number")
     */
    public function checkoutUpdatePhoneNumber(Request $request){

        $user_id = $request->request->get('user_id');
        $data = [];
        $user = $this->userRepo->findOneById($user_id);
        if($user){

            $phone = $user->getPhone();

            $data['phone'] = $phone;

        } else {
            $data['phone'] = '';
        }

        return new JsonResponse($data);

    }

}
