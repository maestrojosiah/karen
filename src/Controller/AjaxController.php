<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\CommentReply;
use App\Entity\User;
use App\Entity\UserSetting;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;
use App\Repository\ProductRepository;
use App\Repository\SettingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Mailer;
use Twig\Environment;
use App\Repository\UserRepository;
use App\Repository\UserSettingRepository;
use Knp\Snappy\Pdf;
use App\Entity\OrderItem;
use App\Manager\CartManager;

// use App\Entity\Specialty;

class AjaxController extends AbstractController
{
    private $snappy_pdf;

    public function __construct(Pdf $snappy_pdf){
        $this->snappy_pdf = $snappy_pdf;
    }    

    /**
     * @Route("/ajax", name="ajax")
     */
    public function index(): Response
    {
        return $this->render('ajax/index.html.twig', [
            'controller_name' => 'AjaxController',
        ]);
    }

    /**
     * @Route("/homepage/contact/form/test/two", name="contactformsending")
     */
    public function contactForm(Request $request, Mailer $mailer, Environment $twig, UserRepository $userRepo): Response
    {        
        
        // $fname = $request->request->get('fname');
        // $lname = $request->request->get('lname');
        // $email = $request->request->get('email');
        // $subject = $request->request->get('subject');
        // $message = $request->request->get('message');
        $maildata = ['name' => 'Josiah Birai', 'emailAd' => 'jshbr7@gmail.com', 'subject' => "Testing mail", 'message' => "Hello there, I'm testing mailing for this system"];

        // $admins = $userRepo->findByUsertype('admin');
        
        $mailer->sendEmailMessage($maildata, 'jshbr7@gmail.com', 'Test email', "contactform.html.twig", "necessary", "contact_form");    
        
        // $mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig", "necessary", "contact_form");    

        
        $this->addFlash(
            'success',
            'The message was sent successfully.',
        );
        return new JsonResponse( 'test' );

    }

    /**
     * @Route("/upload/profile/picture", name="save_profile_picture")
     */
    public function saveFile(Request $request)
    {
        $file = $_POST;
        $path_to_save = 'profile_img_directory';
        if($file){

            $current_photo_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().".png";
            $short_profile_img_directory = $this->getParameter('short_profile_img_directory').time().$this->getUser()->getId().".png";
            $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            file_put_contents($current_photo_path, $data);

            $user = $this->getUser();

            $prefix_to_delete = $this->getParameter('prefix_to_delete');
            $existing_img = $prefix_to_delete . $user->getPhoto();
            if(is_file($existing_img) && file_exists($existing_img)){ unlink($existing_img); }

            $user->setPhoto($short_profile_img_directory);
            $this->save($user);             
            return new JsonResponse($short_profile_img_directory);
            
        }
        // return new JsonResponse("success");
    }

    /**
     * @Route("/save/user/bio", name="savebio")
     */
    public function savebio(Request $request): Response
    {
        $user = $this->getUser();

        $phone = $request->request->get('phone');
        $fname = $request->request->get('fname');
        $lname = $request->request->get('lname');
        $email = $request->request->get('email');
        $about = $request->request->get('about');

        $user->setFname($fname);
        $user->setLname($lname);
        $user->setPhone($phone);
        $user->setEmail($email);
        $user->setAbout($about);

        $this->save($user);


        return new JsonResponse($_POST);

    }

    /**
     * @Route("/add/to/cart", name="add_to_cart")
     */
    public function addToCart(Request $request, ProductRepository $productRepo, CartManager $cartManager): Response
    {
        $user = $this->getUser();

        $item = new OrderItem();

        $product_id = $request->request->get('product_id');
        $product = $productRepo->findOneById($product_id);

        $quantity = $request->request->get('quantity');
        $item->setQuantity($quantity);
        $item->setProduct($product);

        $cart = $cartManager->getCurrentCart();
        $cart
            ->addItem($item)
            ->setUpdatedAt(new \DateTime());

        $cartManager->save($cart);
        $data = [];
        $data['count'] = count($cart->getItems());
        $data['total'] = $cart->getTotal();
        $data['product'] = $product->getName();
        $data['quantity'] = $quantity;
        $data['price'] = $product->getPrice();
        $data['image'] = $product->getImage();
        $data['id'] = $cart->getId();
    
        return new JsonResponse($data);

    }

    // /**
    //  * @Route("/save/user/skill", name="saveskill")
    //  */
    // public function saveskill(Request $request): Response
    // {
    //     $user = $this->getUser();

    //     $title = $request->request->get('title');
    //     $level = $request->request->get('level');
        
    //     $specialty = new Specialty();

    //     $specialty->setTitle($title);
    //     $specialty->setLevel($level);
    //     $specialty->setUser($user);

    //     $this->save($specialty);

    //     return new JsonResponse($level);

    // }


    /**
     * @Route("/save/user/extras", name="saveextras")
     */
    public function saveExtras(Request $request): Response
    {
        $user = $this->getUser();

        $path_to_save = 'resume_directory';

        $smfb = $request->request->get('facebook');
        $smtw = $request->request->get('twitter');
        $smli = $request->request->get('linkedin');
        $smin = $request->request->get('instagram');


        $current_resume_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().".pdf";
        $short_resume_directory = $this->getParameter('short_resume_directory').time().$this->getUser()->getId().".pdf";
        $data = $_FILES['resume']['tmp_name'];
        $filename = $_FILES['resume']['name'];
        // file_put_contents($current_resume_path, $data);
        move_uploaded_file($data, $current_resume_path);

        $prefix_to_delete = $this->getParameter('prefix_to_delete');
        $existing_resume = $prefix_to_delete . $user->getResume();
        if(is_file($existing_resume) && file_exists($existing_resume)){ unlink($existing_resume); }

        $user->setResume($short_resume_directory);
        $user->setFacebook($smfb);
        $user->setTwitter($smtw);
        $user->setLinkedin($smli);
        $user->setInstagram($smin);

        $this->save($user);             


        return new JsonResponse($_FILES);

    }

    /**
     * @Route("/save/top/slide", name="saveextras")
     */
    public function saveTopSlide(Request $request, SettingRepository $settingRepo, UserSettingRepository $uSettingsRepo): Response
    {
        $slider_id = $request->request->get('slider_id');
        $formstring = "slider_".$slider_id;
        $user = $this->getUser();
        $setting = $settingRepo->findOneById($slider_id);
        $u_settings = $uSettingsRepo->findOneBySetting($setting);
        if(null !== $u_settings ){
            $u_setting = $u_settings;
            $test = 'exists';
        } else {
            $u_setting = new UserSetting();
            $test = 'new';
        }

        $path_to_save = 'resume_directory';

        $current_resume_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().".jpg";
        $short_resume_directory = $this->getParameter('short_resume_directory').time().$this->getUser()->getId().".jpg";
        $data = $_FILES["$formstring"]['tmp_name'];
        $filename = $_FILES["$formstring"]['name'];
        // file_put_contents($current_resume_path, $data);
        move_uploaded_file($data, $current_resume_path);

        if($test == 'exists'){
            $prefix_to_delete = $this->getParameter('prefix_to_delete');
            $existing_resume = $prefix_to_delete . $u_setting->getValue();
            if(is_file($existing_resume) && file_exists($existing_resume)){ unlink($existing_resume); }
    
        }

        $u_setting->setValue($short_resume_directory);
        $u_setting->setUser($user);
        $u_setting->setSetting($setting);

        $this->save($u_setting);             


        return new JsonResponse($test);

    }

    /**
     * @Route("/save/user/comment", name="savecomment")
     */
    public function saveComment(Request $request, PostRepository $postRepo): Response
    {
        $dt = [];
        $email = $request->request->get('email');
        $name = $request->request->get('name');
        $content = $request->request->get('comment');
        $post = $postRepo->findOneById($request->request->get('post_id'));
        $comment = new Comment();
        $comment->setContent($content);
        $comment->setCreatedAt(new \DateTime());
        $comment->setName($name);
        $comment->setEmail($email);
        $comment->setPost($post);

        $this->save($comment);
        // send email to concerned parties
        $dt['name'] = $name;
        $dt['content'] = $content;
        $dt['time'] = $comment->getCreatedAt()->format("F j, Y \a\t h:i a");
        $dt['id'] = $comment->getId();
        return new JsonResponse($dt);

    }

    /**
     * @Route("/save/comment/reply", name="save_reply")
     */
    public function saveReply(Request $request, CommentRepository $commentRepo): Response
    {
        $dt = [];
        $r_email = $request->request->get('r_email');
        $r_name = $request->request->get('r_name');
        $content = $request->request->get('reply');
        $comment = $commentRepo->findOneById($request->request->get('comment_id'));
        $reply = new CommentReply();
        $reply->setContent($content);
        $reply->setCreatedAt(new \DateTime());
        $reply->setRName($r_name);
        $reply->setREmail($r_email);
        $reply->setComment($comment);

        $this->save($reply);
        // send email to concerned parties
        $dt['name'] = $r_name;
        $dt['content'] = $content;
        $dt['time'] = $reply->getCreatedAt()->format("F j, Y \a\t h:i a");
        $dt['id'] = $comment->getId();
        return new JsonResponse($dt);

    }

    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    public function delete($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();
    }

       /**
     * @Route("/test/test/test", name="multitesting")
     */
    public function testingmultiplethings(): Response
    {
        $arr = ["config/packages/twig.yaml", "src/Controller/AjaxController.php", "public/dist/assets/css/foundation-emails.css", "public/img/logo-light.png", "templates/emails/contactform.html.twig", "templates/emails/contactreceived.html.twig", "templates/emails/emailBase.html.twig"];
        $proj_dir = $this->getParameter('proj_dir');
        $test = [];
        foreach($arr as $a){
            $src = $proj_dir . "/" . $a;
            $sections = explode("/", $a);
            $file = array_pop($sections);
            $ext = explode(".", $file, 2)[1];
            $destfolder = $proj_dir . "/test";
            foreach ($sections as $section ) {
                $destfolder .= "/".$section;
            }
            if (!file_exists($destfolder)) {
                mkdir($destfolder, 0775, true);
            }            
            $dest = $destfolder . "/" . $file;
            copy($src, $destfolder . "/" . $file);

            $test[] = $dest;
        }
            
        return $this->render('default/test2.html.twig', [
            'arr' => $arr,
            'statements' => $test,
        ]);
    }

 
}
