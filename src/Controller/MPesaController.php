<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Callback;
use App\Manager\CartManager;
use App\Repository\CallbackRepository;
use App\Service\MPesaManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class MPesaController extends AbstractController
{

    private $mpesa;
    private $cbRepo;

    public function __construct(MPesaManager $mpesa, CallbackRepository $cbRepo){
        $this->mpesa = $mpesa;
        $this->cbRepo = $cbRepo;
    }    

    /**
     * @Route("/m/pesa", name="m_pesa")
     */
    public function index(): Response
    {
        return $this->render('m_pesa/index.html.twig', [
            'controller_name' => 'MPesaController',
        ]);
    }

    /**
     * @Route("/m/pesa/auth", name="m_pesa_auth")
     */
    public function auth(): Response
    {
        $response = $this->mpesa->auth();
        return new JsonResponse($response);
    }

    /**
     * @Route("/m/pesa/simulate", name="m_pesa_simulate")
     */
    public function pushSimulate(Request $request): Response
    {
        $amount = $request->request->get('amount');
        $number = $request->request->get('number');
        $reference = $request->request->get('reference');

        list($status, $res) = $this->mpesa->pushSimulate($amount, $number, $reference);
        return new JsonResponse(['status' => $status, 'response' => $res]);
    }

    /**
     * @Route("/make/online/payment/push", name="make_payment_from_website")
     */
    public function stkPush(Request $request)
    {

        // $amount = (int) trim($request->request->get('amount'));
        $amount = 1;
        $number = trim($request->request->get('number'));
        $reference = $request->request->get('reference');
        $description = $request->request->get('description');

        list($status, $res) = $this->mpesa->pushSTK($amount, $number, $reference, $description);
        return new JsonResponse(['status' => $status, 'response' => $res]);
        // return new JsonResponse([$amount, $number, $reference, $description]);
    }

    /**
     * @Route("/checkout/submit/payment", name="checkout_submit_payment")
     */
    public function submitPayment(EntityManagerInterface $em, CartManager $cartManager, Request $request)
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            // encode the plain password
            // echo "<pre>";
            // get the form data

            // $amount = (int) trim($request->request->get('amount'));
            $order_id = $_POST['order_id'];
            $amountToPay = $_POST['amountToPay'];
            $number = $_POST['phone'];
            $reference = "CS-ORDER-$order_id";
            $description = "CS-ORDER-$order_id";

            $amount = 1;
            list($status, $res) = $this->mpesa->pushSTK($amount, $number, $reference, $description);
            // list($status, $res) = ["pending", "ws_CO_21122022220452114705285959"];
            // return new JsonResponse(['status' => $status, 'response' => $res]);
    
            // print_r($status);
            // print_r($res);
            // die();


            // $order = $this->getSqlResult($em, "SELECT * FROM `order` WHERE id = " . $order_id);

            // $updateOrder = $this->getSqlResult($em, "UPDATE `order` SET shippingfee = '" . $shippingFee . "' WHERE id = " . $order[0]['id']);    
            
            return $this->redirectToRoute('checkout_confirm_payment', [
                'amountToPay' => $amountToPay,
                'order_id' => $order_id,
                'status' => $status,
                'request_id' => $res,
            ]);
            // return $this->render('cart/pay.html.twig', [
            //     'cart' => $cartManager->getCurrentCart(),
            //     'amountToPay' => $amountToPay,
            //     'order_id' => $order_id,
            //     'status' => $status,
            //     'request_id' => $res,
            // ]);

        }
    }

    /**
     * @Route("/checkout/confirm/payment/{amountToPay}/{order_id}/{status}/{request_id}", name="checkout_confirm_payment")
     */
    public function confirmPayment($amountToPay, $order_id, $status, $request_id): Response
    {

        return $this->render('cart/pay.html.twig', [
            'amountToPay' => $amountToPay,
            'order_id' => $order_id,
            'status' => $status,
            'request_id' => $request_id,
        ]);


    }

    /**
     * @Route("/checkout/complete/payment", name="checkout_complete_payment")
     */
    public function completePayment(EntityManagerInterface $em)
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            $request_id = $_POST['request_id'];
            $purpose = "CS-ORDER-" . $_POST['order_id'];
    
            list($status, $res) = $this->mpesa->checkStatus($request_id, $purpose);
            

            if($status == 'success'){
                $paymentStatus = "successful";
                $this->followUp($request_id);
                $updateOrder = $this->getSqlResult($em, "UPDATE `order` SET status = 'order' WHERE id = " . $_POST['order_id']);    
            } else {
                $paymentStatus = "not successful";
            }

            return $this->render('cart/status.html.twig', [
                'status' => $status,
                'paymentStatus' => $paymentStatus,
                'request_id' => $request_id,
            ]);
        

        }


    }


    /**
     * @Route("/pmt/stk_push/check/status", name="leepahnapush_status")
     */
    public function checkStatus(Request $request) {

        $request_id = $request->request->get('request_id');
        $purpose = $request->request->get('purpose');

        list($status, $res) = $this->mpesa->checkStatus($request_id, $purpose);
        if($status == 'success'){
            $this->followUp($request_id);
        }
        return new JsonResponse(['status' => $status, 'response' => $res]);


    }

    public function followUp($request_id){

        $callback = $this->cbRepo->findOneByCheckoutRequestID($request_id);

        $json = $callback->getCallbackmetadata();

        $Amount = $this->mpesa->getVar($json, 'Amount', 3);
        $MpesaReceiptNumber = $this->mpesa->getVar($json, 'MpesaReceiptNumber', 3);
        $TransactionDate = $this->mpesa->getVar($json, 'TransactionDate', 3);
        $PhoneNumber = $this->mpesa->getVar($json, 'PhoneNumber', 3);
    
        $callback->setMpesaReceiptNumber($MpesaReceiptNumber);
        $callback->setTransactionDate($TransactionDate);
        $callback->setAmount($Amount);
        $callback->setPhoneNumber($PhoneNumber);
        $this->save($callback);        

        return new JsonResponse('true');

    }

    /**
     * @Route("/touch/script/pmt", name="callback")
     */
    public function callBack() {
               
        if($json = json_decode(file_get_contents("php://input"), true)) {
            $CheckoutRequestID = $this->mpesa->getVar($json, 'CheckoutRequestID', 2);
            $callback = new Callback();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $this->save($callback);        
        } else {
            $json = $_POST;
            $CheckoutRequestID = $this->mpesa->getVar($json, 'CheckoutRequestID', 2);
            $callback = new Callback();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $this->save($callback);        
        }
        
        return new JsonResponse('true');

    }

    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }


    public function getSqlResult($em, $sql)
    {   
    
        $stmt = $em->getConnection()->prepare($sql);
        $result = $stmt->executeQuery()->fetchAllAssociative();
        return $result;

    }


    /**
     * @Route("/test/online/payment/push", name="test_payment_from_website")
     */
    public function testpay(Request $request)
    {

        // $amount = (int) trim($request->request->get('amount'));
        $eventName = "TestEvent";

        return $this->render('default/pay.html.twig', [
            'eventName' => $eventName,
            'eventId' => 54,
        ]);
    }

}
