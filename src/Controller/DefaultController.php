<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Repository\ProductRepository;
use App\Repository\SettingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\AddToCartType;
use App\Service\Sendy;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(CategoryRepository $categoryRepo, ProductRepository $productRepo, PostRepository $blogRepo): Response
    {
        $form = $this->createForm(AddToCartType::class);

        $user = $this->getUser();
        $categories = $categoryRepo->findByPosition();
        $featuredcategories = $categoryRepo->findFeatured();
        $products = $productRepo->findAll();
        $featuredProduct = $productRepo->findOneBy(
            array('featured' => true),
            array('id' => 'ASC')
        );
        if(null == $featuredProduct){
            $featuredProduct = $productRepo->findOneBy(
                array('featured' => null),
                array('id' => 'DESC')
            );
        }
        $blogPosts = $blogRepo->findFiveLatestPosts();

        $data = [];

        $data['categories'] = $categories;
        $data['featuredCategories'] = $featuredcategories;
        $data['products'] = $products;
        $data['featuredProduct'] = $featuredProduct;
        $data['blogPosts'] = $blogPosts;
        return $this->render('default/index.html.twig', [
            'data' => $data,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/test/page", name="testpage")
     */
    public function test(Sendy $sendy): Response
    {
        $inputs = ['Shea Hot 120' => '2', 'Afro Gold 250' => '3', 'Whip Lasting 400' => '1', 'Afro Gold 60' => '2'];

        $data = "{ \r\n \"api_username\": \"B-PGA-8166\", \r\n \"api_key\": \"rjkGbbmhK8NVKPZdqXhMFbrwB2uAAsqfbPrnXFRCfEXeFmderkBzFWVJYnrU\", \r\n \"stock_levels\": \"available\", \r\n \"limit\": \"1000\", \r\n \"page\": \"0\" \r\n }";

        try {
            $response = $sendy->CallAPI('POST', 'https://fulfillment-api.sendyit.com/v2/products', $data );
        } catch (HttpException $ex) {
            echo $ex;
        }
        // get all the products and variants
            
        // make a php array of products key=>array : id->name
        $array = $sendy->jsonToAssocArray($response);
        echo "<pre>";
        $message = $array['message'];
        $products = $array['data'];
        $product_names = [];
        $product_entity = [];
        $words = [];
        $prods = [];
        $ps = [];

        foreach($products as $product){
            $product_id = $product['product_id'];
            $product_name = $product['product_name'];
            $product_variants = $product['product_variants'];
            // echo "<br/>";

            // if it has variants, add to product name array
            if(count($product_variants) > 0){
                $variant_names = [];
                foreach($product_variants as $pv){
                    $pv_id = $pv['product_variant_id'];
                    $product_entity[$pv_id] = ['product_id' => $product_id, 'product_variant_id' => $pv_id, 'quantity' => $pv['product_variant_stock_levels']['available'], 'currency' => 'KES', 'unit_price' => $pv['product_variant_unit_price']];
                    $product_names[$pv_id] = $pv['product_variant_description'] .'_'. $pv['product_variant_quantity'] .'_'. $pv['product_variant_quantity_type'] .'_'. $pv['product_variant_unit_price'] .'_'. $pv['product_variant_stock_levels']['available'].'_'.$product_id;
                    $words[$pv_id] = $pv['product_variant_description'].'_'.$pv['product_variant_quantity'];
                }
            }
        }
        // echo "words<br/>";
        // print_r($words);
        echo "product entitites <br/>";
        // get the products selected by online customer. make an array of products names
        // print_r($product_entity);
        // die();
        // find the products in the database
        print_r(count($inputs));
        foreach ($inputs as $input => $qty) {

            $searchFor = $input;
            $s = strtoupper($searchFor);
            $array_of_words = explode(" ", $s);
            $closest = "";
    
            foreach($product_names as $id => $p){
                $productName = strtoupper($p);
                if($this->str_contains_multiple_words($productName, $array_of_words)){
                    $closest = $productName . "_" . $id;
                    $pvId = trim(explode("_", $closest)[6]);
                } 

                // else {
                //     echo $productName . " does not have " . implode(",", $array_of_words) . "<br/>";
                // }
            }
            
            // print_r($product_entity[$pvId]);
            $entity = $product_entity[$pvId];
            $entity['quantity'] = $qty;
            // print_r($entity);
            $prods[] = $entity;
            $ps[] = $product_names[$pvId];
    
                
        }

        // print_r($product_entity[$pvId]);
        $array_for_json = ['api_username' => 'B-PGA-8166', 'api_key' => 'rjkGbbmhK8NVKPZdqXhMFbrwB2uAAsqfbPrnXFRCfEXeFmderkBzFWVJYnrU', 'order_type' => 'DELIVERY', 'products' => $prods ];

        $json_string_for_request = json_encode($array_for_json);

        try {
            $responseSP = $sendy->CallAPI('POST', 'https://fulfillment-api.sendyit.com/v2/orders/fee', $json_string_for_request );
        } catch (HttpException $ex) {
            echo $ex;
        }

        print_r($responseSP);
        print_r($ps);
        print_r($prods);
        print_r($product_names);
        
        // echo "product names: <br/>";
        // print_r($product_names);

        // print_r($products);
        die();
        return $this->render('default/test.html.twig', [
            'data' => 'data',
        ]);
    
    }
    public function str_contains_multiple_words($string, $array_of_words)
    {
       if (!$array_of_words) return false;
       
       $i = 0;
       
       foreach ($array_of_words as $words)
       {
            if (strpos($string, $words) !== FALSE) $i++;
       }
       
       return ($i == count($array_of_words)) ? true : false;
    }

    /**
     * @Route("/crystal/shop/{category}", name="shop")
     */
    public function shop($category, ProductRepository $productRepo, CategoryRepository $categoryRepo): Response
    {

        // error_reporting(E_ALL);
        // ini_set("display_errors", 1);
        $featuredProduct = $productRepo->findOneBy(
            array('featured' => true),
            array('id' => 'ASC')
        );
        if(null == $featuredProduct){
            $featuredProduct = $productRepo->findOneBy(
                array('featured' => null),
                array('id' => 'DESC')
            );
        }
        $categories = $categoryRepo->findAll();
        if($category == "all"){
            $cat = "All";
            $products = $productRepo->findAll();
        } else {
            $cat = $categoryRepo->findOneById($category);
            $products = $productRepo->findByCategory($cat);
        }
        $data = [];
        $data['categories'] = $categories;
        $data['products'] = $products;
        $data['featuredProduct'] = $featuredProduct;
        $data['category'] = $cat;

        return $this->render('default/shop.html.twig', [
            'data' => $data,
        ]);
    }
}
