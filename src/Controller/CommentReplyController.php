<?php

namespace App\Controller;

use App\Entity\CommentReply;
use App\Form\CommentReplyType;
use App\Repository\CommentReplyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment/reply")
 */
class CommentReplyController extends AbstractController
{
    /**
     * @Route("/", name="comment_reply_index", methods={"GET"})
     */
    public function index(CommentReplyRepository $commentReplyRepository): Response
    {
        return $this->render('comment_reply/index.html.twig', [
            'comment_replies' => $commentReplyRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="comment_reply_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $commentReply = new CommentReply();
        $form = $this->createForm(CommentReplyType::class, $commentReply);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($commentReply);
            $entityManager->flush();

            return $this->redirectToRoute('comment_reply_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('comment_reply/new.html.twig', [
            'comment_reply' => $commentReply,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="comment_reply_show", methods={"GET"})
     */
    public function show(CommentReply $commentReply): Response
    {
        return $this->render('comment_reply/show.html.twig', [
            'comment_reply' => $commentReply,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="comment_reply_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CommentReply $commentReply): Response
    {
        $form = $this->createForm(CommentReplyType::class, $commentReply);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comment_reply_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('comment_reply/edit.html.twig', [
            'comment_reply' => $commentReply,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="comment_reply_delete", methods={"POST"})
     */
    public function delete(Request $request, CommentReply $commentReply): Response
    {
        if ($this->isCsrfTokenValid('delete'.$commentReply->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($commentReply);
            $entityManager->flush();
        }

        return $this->redirectToRoute('comment_reply_index', [], Response::HTTP_SEE_OTHER);
    }
}
