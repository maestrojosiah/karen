<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Repository\ProductRepository;
use App\Repository\SettingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(ProductRepository $productRepo, CategoryRepository $categoryRepo, PostRepository $postRepo, SettingRepository $settingsRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $user = $this->getUser();
        if($user->getUsertype() != 'admin'){
            return $this->redirectToRoute('default');
        }
        $productsCount = $productRepo->countProducts();
        $categoriesCount = $categoryRepo->countCategories();
        $postCount = $postRepo->countPosts();
        $settingsCount = $settingsRepo->countSettings();
        return $this->render('admin/index.html.twig', [
            'productsCount' => $productsCount,
            'categoriesCount' => $categoriesCount,
            'postCount' => $postCount,
            'settingsCount' => $settingsCount,
        ]);
    }

    /**
     * @Route("/admin/profile", name="profile")
     */
    public function profile(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $admin = $this->getUser();
        return $this->render('admin/profile.html.twig', [
            'admin' => $admin,
        ]);
    }
        
}
