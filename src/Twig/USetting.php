<?php 

namespace App\Twig;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class USetting extends AbstractExtension implements GlobalsInterface 
{
    protected $em;

    public function __construct(EntityManagerInterface $em, UserRepository $ur)
    {
       $this->em = $em;
       $this->ur = $ur;
    }
 
    
    public function getGlobals(): array
    {
        $user = $this->ur->findOneBy(
            ['usertype' => 'admin'],
            ['id' => 'ASC']
        );
        $u_settings = [];
        if($user && $user->getUserSettings()){

            foreach ($user->getUserSettings() as $key => $s) {
                $u_settings[$s->getSetting()->getDescription()] = $s->getValue();
            }

        } else {
            $u_settings[] = null;
        }
        return [
            'u_settings' => $u_settings
        ];
 
    }
    
}