<?php 

namespace App\Twig;

use Doctrine\ORM\EntityManagerInterface;
use App\Manager\CartManager;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class Variables extends AbstractExtension implements GlobalsInterface 
{
    protected $em;

    public function __construct(CartManager $cm)
    {
       $this->cm = $cm;
    }
 
    
    public function getGlobals(): array
    {
        return [
            'cart' => $this->cm->getCurrentCart()
        ];
 
    }
    
}