<?php 

namespace App\Twig;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class Contact extends AbstractExtension implements GlobalsInterface 
{
    protected $em;

    public function __construct(EntityManagerInterface $em, UserRepository $ur)
    {
       $this->em = $em;
       $this->ur = $ur;
    }
 
    
    public function getGlobals(): array
    {
        return [
            'contact' => $this->ur->findOneBy(['usertype' => 'admin'])
        ];
 
    }
    
}