-- MySQL dump 10.13  Distrib 8.0.31, for Linux (x86_64)
--
-- Host: localhost    Database: karen
-- ------------------------------------------------------
-- Server version	8.0.31-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `callback`
--

DROP TABLE IF EXISTS `callback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `callback` (
  `id` int NOT NULL AUTO_INCREMENT,
  `checkout_request_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mpesa_receipt_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `callbackmetadata` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `callback`
--

LOCK TABLES `callback` WRITE;
/*!40000 ALTER TABLE `callback` DISABLE KEYS */;
INSERT INTO `callback` VALUES (1,'ws_CO_13122022190720584705285959',NULL,NULL,NULL,NULL,NULL,'{\"Body\": {\"stkCallback\": {\"ResultCode\": 2028, \"ResultDesc\": \"The request is not permitted according to product assignment.\", \"CheckoutRequestID\": \"ws_CO_13122022190720584705285959\", \"MerchantRequestID\": \"15620-78591475-2\"}}}'),(2,'ws_CO_13122022195708438705285959',NULL,NULL,NULL,NULL,NULL,'{\"Body\": {\"stkCallback\": {\"ResultCode\": 2028, \"ResultDesc\": \"The request is not permitted according to product assignment.\", \"CheckoutRequestID\": \"ws_CO_13122022195708438705285959\", \"MerchantRequestID\": \"32582-82977930-1\"}}}'),(3,'ws_CO_13122022195955410705285959',NULL,NULL,NULL,NULL,NULL,'{\"Body\": {\"stkCallback\": {\"ResultCode\": 2028, \"ResultDesc\": \"The request is not permitted according to product assignment.\", \"CheckoutRequestID\": \"ws_CO_13122022195955410705285959\", \"MerchantRequestID\": \"32581-82989240-1\"}}}'),(4,'ws_CO_13122022201520144705285959',NULL,NULL,NULL,NULL,NULL,'{\"Body\": {\"stkCallback\": {\"ResultCode\": 2028, \"ResultDesc\": \"The request is not permitted according to product assignment.\", \"CheckoutRequestID\": \"ws_CO_13122022201520144705285959\", \"MerchantRequestID\": \"32478-4741426-2\"}}}'),(5,'ws_CO_13122022202510726705285959',NULL,NULL,NULL,NULL,NULL,'{\"Body\": {\"stkCallback\": {\"ResultCode\": 2028, \"ResultDesc\": \"The request is not permitted according to product assignment.\", \"CheckoutRequestID\": \"ws_CO_13122022202510726705285959\", \"MerchantRequestID\": \"118496-79008598-1\"}}}'),(6,'ws_CO_13122022202700892705285959',NULL,NULL,NULL,NULL,NULL,'{\"Body\": {\"stkCallback\": {\"ResultCode\": 0, \"ResultDesc\": \"The service request is processed successfully.\", \"CallbackMetadata\": {\"Item\": [{\"Name\": \"Amount\", \"Value\": 1}, {\"Name\": \"MpesaReceiptNumber\", \"Value\": \"QLD747H19J\"}, {\"Name\": \"Balance\"}, {\"Name\": \"TransactionDate\", \"Value\": 20221213202709}, {\"Name\": \"PhoneNumber\", \"Value\": 254705285959}]}, \"CheckoutRequestID\": \"ws_CO_13122022202700892705285959\", \"MerchantRequestID\": \"5473-80170902-1\"}}}'),(7,'ws_CO_13122022203257021705285959',NULL,NULL,NULL,NULL,NULL,'{\"Body\": {\"stkCallback\": {\"ResultCode\": 0, \"ResultDesc\": \"The service request is processed successfully.\", \"CallbackMetadata\": {\"Item\": [{\"Name\": \"Amount\", \"Value\": 1}, {\"Name\": \"MpesaReceiptNumber\", \"Value\": \"QLD54852ZV\"}, {\"Name\": \"Balance\"}, {\"Name\": \"TransactionDate\", \"Value\": 20221213203304}, {\"Name\": \"PhoneNumber\", \"Value\": 254705285959}]}, \"CheckoutRequestID\": \"ws_CO_13122022203257021705285959\", \"MerchantRequestID\": \"73206-55805663-2\"}}}');
/*!40000 ALTER TABLE `callback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (4,'Lip Care','4','IMGL9292-614c73aed4929.jpg'),(5,'Whipped Body Butter','1','IMGL9257-614c72cb12643.jpg'),(6,'Moisturizers','2','IMGL9319-614c731e52243.jpg'),(7,'Body Oils','3','IMGL9266-614c73435437b.jpg'),(8,'Hair Butter','5','IMGL9261-614c73db0d41b.jpg'),(9,'Hair Oil','6','IMGL9503-614c7437edd1c.jpg'),(10,'Liquid Black Soap','7','IMGL9277-614c7465e0dcf.jpg');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` int DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526C4B89032C` (`post_id`),
  CONSTRAINT `FK_9474526C4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_reply`
--

DROP TABLE IF EXISTS `comment_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment_reply` (
  `id` int NOT NULL AUTO_INCREMENT,
  `comment_id` int DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `r_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_54325E11F8697D13` (`comment_id`),
  CONSTRAINT `FK_54325E11F8697D13` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_reply`
--

LOCK TABLES `comment_reply` WRITE;
/*!40000 ALTER TABLE `comment_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `faq` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gallery` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_472B783A4584665A` (`product_id`),
  CONSTRAINT `FK_472B783A4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int DEFAULT NULL,
  `ordernotes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingfee` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F5299398A76ED395` (`user_id`),
  CONSTRAINT `FK_F5299398A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,'cart','2021-10-04 18:55:45','2021-10-04 18:56:35',NULL,NULL,NULL),(2,'cart','2021-10-11 18:29:18','2021-10-11 19:19:03',NULL,NULL,NULL),(3,'cart','2021-10-12 06:25:11','2021-10-12 06:26:10',NULL,NULL,NULL),(4,'cart','2021-10-15 07:00:20','2021-10-15 07:00:20',NULL,NULL,NULL),(5,'cart','2022-05-31 16:56:41','2022-05-31 16:56:41',NULL,NULL,NULL),(6,'cart','2022-07-10 23:38:08','2022-07-10 23:39:34',NULL,NULL,NULL),(7,'cart','2022-08-02 05:58:37','2022-08-02 05:58:37',NULL,NULL,NULL),(8,'cart','2022-11-25 08:51:49','2022-11-25 08:51:49',NULL,NULL,NULL),(9,'cart','2022-12-13 10:38:13','2022-12-13 10:38:23',NULL,NULL,NULL),(10,'cart','2022-12-19 20:06:54','2022-12-20 20:32:58',3,'Turn left at maziwa stage','238'),(11,'cart','2022-12-20 13:25:37','2022-12-20 13:25:41',NULL,NULL,NULL);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_item` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int NOT NULL,
  `order_ref_id` int NOT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_52EA1F094584665A` (`product_id`),
  KEY `IDX_52EA1F09E238517C` (`order_ref_id`),
  CONSTRAINT `FK_52EA1F094584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_52EA1F09E238517C` FOREIGN KEY (`order_ref_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item`
--

LOCK TABLES `order_item` WRITE;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` VALUES (1,1,1,3),(6,21,3,2),(7,18,3,1),(8,17,3,1),(9,14,4,1),(10,21,5,1),(11,11,6,1),(13,1,7,1),(14,3,8,1),(15,3,9,1),(16,9,9,1),(17,3,10,1),(18,9,10,2),(19,4,10,1);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pmt`
--

DROP TABLE IF EXISTS `pmt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pmt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `merchantrequestid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checkoutrequestid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsecode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsedescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customermessage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resultcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resultdesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pmt`
--

LOCK TABLES `pmt` WRITE;
/*!40000 ALTER TABLE `pmt` DISABLE KEYS */;
INSERT INTO `pmt` VALUES (1,'6083-102297304-2','ws_CO_21122022091637529702825682','0','Success. Request accepted for processing','Success. Request accepted for processing',NULL,NULL),(2,'32492-53266666-1','ws_CO_21122022092204986705285959','0','Success. Request accepted for processing','Success. Request accepted for processing',NULL,NULL);
/*!40000 ALTER TABLE `pmt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5A8A6C8DA76ED395` (`user_id`),
  CONSTRAINT `FK_5A8A6C8DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,1,'A Bun In The Oven - How Crystal Shea Was Born','<p>One windy day mid-2012, I recall vividly walking down the streets of Nairobi looking for something different for a little girl&lsquo;s skin that I was yet to touch or see. I was excited and felt blessed to be carrying her in my belly and the deep concern to give her the best had me walking all over looking &nbsp;for what I thought necessary for her first days here on earth &nbsp;</p>\r\n\r\n<p><img alt=\"Waiting for the first born\" src=\"https://crystalshea.co.ke/bonique/assets/img/blog/Waiting-for-first-born-c.JPG\" style=\"float:left; margin:20px; max-width:100%; width:320px\" /></p>\r\n\r\n<p>At around the same time, I had been reading how many baby products for skin and hair were doing more harm than good to babies&rsquo; skin. So I was on a mission to get something different, something great for my baby and I was convinced there must be a better way.</p>\r\n\r\n<p>It didn&rsquo;t take long to find answers&hellip; but that&rsquo;s after trying a bit of the aqueous cream and common baby lotions.</p>\r\n\r\n<p>Then a weekly email, made me turn to my kitchen for answers. I had some olive oil that I used sparingly for salads. I quickly poured some into a clean jar and couldn&rsquo;t wait for bath time to use it on my newborn&hellip;and the rest as they say is history.</p>\r\n\r\n<p>I later came to learn though that not all organic oils are the same&hellip; and by the time I gave birth to my second born, I had made and packed some natural blend of oils and butter for him in his hospital bag ready to lather on his skin from day one. I was an excited mum. The nurses comment didn&rsquo;t put me down, but just gladly explained I think natural is the best. I had been using the same butters and oils for my stretching skin, and my body was loving it</p>\r\n\r\n<blockquote>\r\n<p>By the time I gave birth to my second born, I had made and packed some natural blend of oils and butter for him in his hospital bag ready to lather on his skin from day one.</p>\r\n</blockquote>\r\n\r\n<p>Every other month I was blending and whipping small batches of Shea butter, coconut oil and some lavender essential oil and by now the whole family was hooked to my homemade body butters and oil. I kept tweaking the recipes and giving my mum any extra jar that I made, that she could use on her hands and body.</p>\r\n\r\n<p>Fast forward 8&nbsp;years later, I appreciate how all my 4 babies have smooth healthy skin and I didn&rsquo;t seem to have to put much effort other than use natural and organic butters and oils. &nbsp;An idea was born during the COVID pandemic in 2020 and when I started by making a few extra body butter batches for neighbors and friends.</p>\r\n\r\n<p><img alt=\"\" src=\"https://crystalshea.co.ke/bonique/assets/img/blog/Crystal-shea-first-products-c.JPG\" style=\"float:left; margin:20px; max-width:100%; width:320px\" /></p>\r\n\r\n<p>My biochemistry background, though helpful wasn&rsquo;t enough to understand what formulating hair and body products entailed. &nbsp;I felt the need to learn more on formulating organic skincare and Centre of Excellence diploma in organic skincare and haircare deepened my understanding and helped me see the endless possibilities and great benefits of using natural and naturally derived products for our body. Not only was the organic skin care industry growing fast, more people were becoming more aware of what they put on their skin and natural and organic products were becoming more popular.</p>\r\n\r\n<p>And I found it necessary to make organic products available to more people, my contribution to a healthy nation.</p>\r\n\r\n<p>Crystal Shea was born.</p>','How-crystal-shea-was-born-61686830c171e.jpg','2021-10-14 17:34:59','2021-10-14 17:34:59'),(2,1,'Business Shower - How Crystal Shea Kicked Off','<p>It had been months of formulating and perfecting my products. I hadn&rsquo;t officially started selling but I knew I needed to start with God and dedicate what was ahead of me, because on most days I didn&rsquo;t know what exactly what I was doing or thinking to do.</p>\r\n\r\n<p>The twins 2nd&nbsp;birthday was drawing close, and in the midst of planning, we thought this will be good time to have our family and a few close friends come over for the birthday lunch and launch of our new baby business, Crystal Shea.</p>\r\n\r\n<p><img alt=\"\" src=\"https://crystalshea.co.ke/bonique/assets/img/blog/Launching-crystal-shea-c.JPG\" style=\"float:left; margin:20px; max-width:100%; width:320px\" /></p>\r\n\r\n<p>The weeks that led to the D-day just flew buy, as I got the desired product packaging, finalized on the labels and made an extra batch of lip balms, body butters and facial oil.</p>\r\n\r\n<p>I indeed felt the pressure to make sure I had the best and enough products for family and friends.</p>\r\n\r\n<p>How can I forget to mention the never ending task of getting product names and how I constantly bother my husband and siblings to counter check the names I came up with.</p>\r\n\r\n<p>The day was finally here, had spent the night doing the last minute touch up, but there was still one thing left to do that morning - putting labels on the finished products. It was exciting and still nerve wrecking holding the two events at once, but once everyone had had lunch, it was now the time to let everyone know the other reason they were here, before we cut the cake. It was a beautiful surprise for many.&nbsp;</p>\r\n\r\n<p>My husband together with a dear friend helped me introduce the product line and needless to say, all the products were sold out. It was the support, love and warmth I needed on the first day of business.</p>\r\n\r\n<blockquote>\r\n<p>I indeed felt the pressure to make sure I had the best and enough products for family and friends.</p>\r\n</blockquote>\r\n\r\n<p><img alt=\"\" src=\"https://crystalshea.co.ke/bonique/assets/img/blog/Twin\'s-second-birthday-c.JPG\" style=\"float:left; margin:20px; max-width:100%; width:320px\" /></p>\r\n\r\n<p>The best part of it were some little notes that all the guests wrote about me, which are my constant affirmations that I can do this. And that&rsquo;s how the business shower took place surrounded with family and friends on the 27th&nbsp;Feb, 2021.</p>\r\n\r\n<p>And we crowned it all with cake and more blessings.</p>','Business-Shower-616869248e402.jpg','2021-10-14 17:42:34','2021-10-14 17:42:34');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `specs` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD12469DE2` (`category_id`),
  CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,9,'Afro Gold','500','Gold','<p>A blend of essential oils for thick healthy hair. It strengthens, softens and promotes hair growth.</p>','web-photos-afrogold-60ml-360x360-6152a1344d6ec.jpg','<p>Shea oil, coconut oil,avocado oil, olive oil, Jamaican black castor oil with Lavender, rosemary and peppermint essential oil.</p>','60 ML','both',NULL),(2,8,'Afro Blend','600','Brown','<p>A combination of Shea butter and avocado oil, olive oil and other organic oils makes it effective in treating the scalp, strengthening hair, and promoting hair growth.</p>','web-photos-afroblend-250g-360x360-6152a11e84d09.jpg','<p>Shea butter, avocado oil, olive oil, coconut oil, Jamaican black castor oil, argan oil with peppermint and rosemary essential oil.&nbsp;</p>','300g','both',NULL),(3,6,'Shea Glam','500','White','<p>Moisturizing body lotion, that keeps skin soft and smooth. Prevents&nbsp;premature aging and dull looking skin therefore helps tighten the skin.</p>','web-photos-shea-glam-250ml-360x360-6152a10a62131.jpg','<p>Ingredients: Water, Shea butter, jojoba oil, Vit E, glycerin with rosemary and geranium essential oils.</p>','250ml','both',1),(4,5,'Whip Defy','1200','White','<p>Helps defy symptoms of skin conditons suh as acne, eczema, psoriasis, dry skin and blemishes.</p>','web-photos-whip-defy-500g-360x360-6152a0ec918fb.jpg','<p>Whipped Shea butter with tea tree essential oil</p>','400g','both',NULL),(6,5,'Whip Defy','500','White','<p>Helps defy symptoms of skin conditons suh as acne, eczema, psoriasis, dry skin and blemishes.</p>','web-photos-whip-defy-250g-360x360-6152a16a8a5b3.jpg','<p>Shea butter with tea tree essential oil</p>','150g','both',NULL),(7,5,'Whip Lasting','1200','White','<p>Hydrates, nourishes the skin and is perfect for dry and rough skin. It also helps minimize apearance of stretch marks and scarring.</p>','web-photos-whiplasting-500g-360x360-6152a1b4ba208.jpg','<p>Shea &amp; Cocoa butter with frankincense essential oil.</p>','500g','both',NULL),(9,5,'Whip Lasting','500','White','<p>Hydrates, nourishes the skin and is perfect for dry and rough skin. It also helps minimize apearance of stretch marks and scarring.</p>','web-photos-whiplasting-250g-360x360-6152a1f17243e.jpg','<p>Shea &amp; Cocoa butter with frankincense essential oil</p>','150g','both',NULL),(10,6,'Shea Glam','100','White','<p>Prevents premature aging and dull looking skin thereforehelping the skin tighten.</p>','web-photos-shea-glam-60ml-360x360-6152a20e87151.jpg','<p>Shea butter, Jojoba oil, Vit E, glycerin with rosemary and geranium essential oils.</p>','60ml','both',NULL),(11,8,'Afro Blend','1000','White','<p>A combination of Shea butter and other organi oils making it more effective in treating the scalp, strengthening hair and promoting hair growth.</p>','web-photos-afroblend-500g-360x360-6152a22d9376e.jpg','<p>Shea butter, coconut oil, Jamaican black castor oil, argan oil with peppermint and rosemary essential oil.</p>','500g','both',NULL),(12,9,'Afro Gold','1500','Gold','<p>A blend eseental oils for thick healthy hair. It also strengthens, softens and promotes hair growth.</p>','web-photos-afrogold-250ml-360x360-6152a67587cf4.jpg','<p>Shea butter, coconut oil, avocado oil, olive oil, Jamaican black castor oil,&nbsp;with lavender peppermint and rosemary essential oil.</p>','250ml','both',NULL),(13,6,'Shea Hot','1000','White','<p>A natural broad spectrum sunscreen lotion that is free from any chemicals and toxic ingredients making it safe to use on baby and sensitive skin.&nbsp;</p>\r\n\r\n<p>The combination of natural oils and minerals gives it an SPF of 30.&nbsp;</p>','web-photos-shea-hot-360x360-6152a292b5fb3.jpg','<p>Shea butter, coconut oil, carrot seed oil, zinc oxide and essential oils</p>\r\n\r\n<p>&nbsp;</p>','120ml','both',NULL),(14,6,'Shea Hot','500','White','<p>A natural broad spectrum sunscreen lotion that is free from any chemicals and toxic ingredients making it safe to use on baby and sensitive skin.&nbsp;</p>\r\n\r\n<p>The combination of natural oils and minerals gives it an SPF of 30.&nbsp;</p>','web-photos-shea-hot-360x360-6152a2e358b76.jpg','<p>Shea butter, coconut oil, carrot seed oil, zinc oxide and essential oils</p>\r\n\r\n<p>&nbsp;</p>','60ml','both',NULL),(15,10,'Liquid African Black Soap','1000','Black','<p>3 in 1 soap that serves as&nbsp;Facial cleanser, Body cleanser, Hair shampoo&nbsp;</p>\r\n\r\n<p>It is an all natural product made using plants and contains Shea butter which helps keep your skin moisturized.&nbsp;It&rsquo;s naturally exfoliating, leaving your skin fresh and cleaner.&nbsp;</p>\r\n\r\n<p>&nbsp;Free from dyes and fragrances making it suitable for all skin types and safe to use on babies.&nbsp;Has antibacterial properties and helps treat acne and reduce impact of ezcema</p>','web-photos-liquid-black-soap-500ml-360x360-6152a4579b405.jpg','<p>African black soap, water, Shea butter, natural emulsifiers</p>','500ml','both',NULL),(16,10,'Liquid African Black Soap','650','Black','<p>3 in 1 soap that serves as&nbsp;Facial cleanser, Body cleanser, Hair shampoo&nbsp;</p>\r\n\r\n<p>It is an all natural product made using plants and contains Shea butter which helps keep your skin moisturized.&nbsp;It&rsquo;s naturally exfoliating, leaving your skin fresh and cleaner.&nbsp;</p>\r\n\r\n<p>&nbsp;Free from dyes and fragrances making it suitable for all skin types and safe to use on babies.&nbsp;Has antibacterial properties and helps treat acne and reduce impact of ezcema</p>','web-photos-liquid-black-soap-250ml-360x360-6152a46d369f3.jpg','<p>African black soap, water, Shea butter, natural emulsifiers&nbsp;</p>','250ml','both',NULL),(17,7,'Crystal Glow','1100','Gold','<p>A blend of essential oils that nourishes and protects the skin. Helps to keep the skin cells hydrated and strong by sealing in water and softens your skin as a result.&nbsp;</p>\r\n\r\n<p>Full of vitamin C and E that help improve the appearance of uneven skin. Helps minimize wrinkles by increasing&nbsp;&nbsp;collagen production in the skin.&nbsp;&nbsp;</p>\r\n\r\n<p>Good for all skin types.&nbsp;</p>','web-photos-crystal-glow-360x360-6152a48a1d636.jpg','<p>Rosehip oil, Argan oil, Jojoba oil, Grapeseed, Sweet almond oil with Lavender, Tea tree and Geranium essential oils.&nbsp;</p>','50ml','women',NULL),(18,7,'Relaxing Massage Oil','2000','Gold','<p>Formulated to offer relief from muscle pain, back pain and for a relaxing body massage.</p>\r\n\r\n<p>The essential oils peppermint and eucalyptus have a cooling effect on the skin and help reduce pain and inflammation.&nbsp;Ginger has a warming effect on sore muscles and helps relieve pain, making it an ideal muscle relaxant massage oil.&nbsp;</p>\r\n\r\n<p>Frankincense and lavender essential oils have the ability to calm and relax your mind and body, helping to reduce tension.&nbsp;The addition of essential oil ylang-ylang&nbsp;&nbsp;gives it a balanced sensual aroma making it great for that romantic massage oil.&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>','web-photos-relaxing-massage-oil-360x360-6152a4a3d7b17.jpg','<p>Olive oil, Coconut oil, Shea oil, Sunflower oil, with peppermint, eucalyptus, ginger, frankincense, lavender and ylang-ylang essential oils&nbsp;</p>\r\n\r\n<p>&nbsp;</p>','250ml','both',NULL),(19,7,'Relaxing Massage Oil','1000','Gold','<p>Formulated to offer relief from muscle pain, back pain and for a relaxing body massage.</p>\r\n\r\n<p>The essential oils peppermint and eucalyptus have a cooling effect on the skin and help reduce pain and inflammation.&nbsp;Ginger has a warming effect on sore muscles and helps relieve pain, making it an ideal muscle relaxant massage oil.&nbsp;</p>\r\n\r\n<p>Frankincense and lavender essential oils have the ability to calm and relax your mind and body, helping to reduce tension.&nbsp;The addition of essential oil ylang-ylang&nbsp;&nbsp;gives it a balanced sensual aroma making it great for that romantic massage oil.&nbsp;&nbsp;</p>','web-photos-relaxing-massage-oil-360x360-6152a515742dc.jpg','<p>Olive oil, Coconut oil, Shea oil, Sunflower oil, with peppermint, eucalyptus, ginger, frankincense, lavender and ylang-ylang essential oils&nbsp;</p>\r\n\r\n<p>&nbsp;</p>','125ml','both',NULL),(20,7,'Calming Body Oil','2000','Gold','<p>Moisturizes and softens the skin. It helps boost your skin&rsquo;s&nbsp;&nbsp;radiance and evens out the skin tone.&nbsp;It&rsquo;s hydrating and can be used for a calming, mild massage.</p>\r\n\r\n<p>Can also be used as an oil cleanser for your face and help clean your pores and&nbsp;&nbsp;in makeup removal leaving your skin hydrated.&nbsp;</p>\r\n\r\n<p>Safe for babies,&nbsp;during pregnancy and suitable for all skin types.</p>','web-photos-calming-body-oil-360x360-6152a53e8f9a7.jpg','<p>Argan oil, Grapeseed oil, Jojoba oil, Shea oil, Rose oil with lavender and tea tree essential oils&nbsp;</p>','250ml','both',NULL),(21,7,'Calming Body Oil','1000','Gold','<p>Moisturizes and softens the skin. It helps boost your skin&rsquo;s&nbsp;&nbsp;radiance and evens out the skin tone.&nbsp;It&rsquo;s hydrating and can be used for a calming, mild massage.</p>\r\n\r\n<p>Can also be used as an oil cleanser for your face and help clean your pores and&nbsp;&nbsp;in makeup removal leaving your skin hydrated.&nbsp;</p>\r\n\r\n<p>Safe for babies,&nbsp;during pregnancy and suitable for all skin types.</p>','web-photos-calming-body-oil-360x360-6152a54f82168.jpg','<p>Argan oil, Grapeseed oil, Jojoba oil, Shea oil, Rose oil with lavender and tea tree essential oils&nbsp;</p>','125ml','women',NULL),(22,4,'Cool peppermint lip balm','200','White','<p>A natural lipbalm with Shea butter that leaves a cool sensation and a peppermint smell.</p>','web-photos-lipbalm-peppermint-360x360-6152a567b2912.jpg','<p>Shea butter, sweet almond oil, bees wax, Vit E with essential oils</p>','5g','both',NULL),(23,4,'Sweet orange lip balm','200','White','<p>A natural lipbalm with Shea and cocoa butter that leaves a sweet orange&nbsp;smell.</p>','products-800x800-lipbalm-sweet-6152a64fdf953.jpg','<p>Shea &amp; Cocoa butter, sweet almond oil, bes wax, Vit E with essential oils</p>','5g','both',NULL),(24,4,'Rose red lip balm','200','Red','<p>A natural tinted lipbalm with Shea and cocoa butter that leaves a pop of red colour.</p>','web-photos-lipbalm-rose-red-360x360-6152a69468abf.jpg','<p>Shea butter, sweet almond oil, bees wax,Vit E with esssential oils.</p>','5g','women',NULL),(25,4,'Peach pink lip balm','200','Pink','<p>A natural tinted lip balm with Shea butter that leaves you with a pop of&nbsp;light pink colour.</p>','web-photos-lipbalm-pink-360x360-6152a6a31cdae.jpg','<p>Shea butter, sweet almond&nbsp;oil, bees wax, Vit E with essential oils</p>','5g','women',NULL),(26,4,'Bold brown lip balm','200','Brown','<p>A natural tinted lip balm with Shea butter that leaves you with a pop of brown&nbsp;colour.</p>','web-photos-lipbalm-brown-360x360-6152a6b8c4771.jpg','<p>Shea butter, sweet almond oil, bees wax, Vit E with essential oils.</p>','5g','women',NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_password_request`
--

DROP TABLE IF EXISTS `reset_password_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reset_password_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `selector` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashed_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `expires_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_7CE748AA76ED395` (`user_id`),
  CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_password_request`
--

LOCK TABLES `reset_password_request` WRITE;
/*!40000 ALTER TABLE `reset_password_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `reset_password_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `review` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `stars` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_on` datetime NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_794381C6A76ED395` (`user_id`),
  KEY `IDX_794381C64584665A` (`product_id`),
  CONSTRAINT `FK_794381C64584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_794381C6A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_usertype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'main_slide_image_1_1920x1054','Main Slide Image 1','website_images','admin'),(2,'main_slide_image_2_1920x1054','Main Slide Image 2','website_images','admin'),(3,'working_hours','Working Hours','information','admin'),(4,'featured_category_1_title','Featured Category Title 1','information','admin'),(5,'shop_now_image_810x450','Shop Now Image','website_images','admin'),(6,'main_slide_top_text_1','First Text on the Main Slide','information','admin'),(7,'main_slide_top_text_2','First Text on the Main Slide 2','information','admin'),(8,'main_slide_title_1','Main Slide Title','information','admin'),(9,'main_slide_title_2','Main Slide Title 2','information','admin'),(10,'main_slide_text_1','Main Slide Text','information','admin'),(11,'main_slide_text_2','Main Slide Text 2','information','admin'),(12,'business_name','Business Name','information','admin'),(13,'business_slogan','Business Slogan','information','admin'),(14,'featured_category_2_title','Featured Category Title 2','information','admin'),(15,'featured_product','Featured Product','featured','admin'),(16,'shop_now_text','Call To Action','action','admin'),(17,'shop_now_top_text','Top Text For Shop Now','information','admin'),(18,'shop_now_title','Title for Shop Now','information','admin'),(19,'featured_category_image_1_915x503','Featured Category Image 1','website_images','admin'),(20,'featured_category_image_2_915x503','Featured Category Image 2','website_images','admin'),(21,'shop_main_banner_1050x486','Shop Top Banner Image','website_images','admin'),(22,'featured_category_1_text','Featured Category Text 1','information','admin'),(23,'featured_category_2_text','Featured Category Text 2','information','admin');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slide`
--

DROP TABLE IF EXISTS `slide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slide` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_72EFEE624584665A` (`product_id`),
  CONSTRAINT `FK_72EFEE624584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slide`
--

LOCK TABLES `slide` WRITE;
/*!40000 ALTER TABLE `slide` DISABLE KEYS */;
/*!40000 ALTER TABLE `slide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tag` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_389B7834584665A` (`product_id`),
  CONSTRAINT `FK_389B7834584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonial`
--

DROP TABLE IF EXISTS `testimonial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testimonial` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimony` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `products` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonial`
--

LOCK TABLES `testimonial` WRITE;
/*!40000 ALTER TABLE `testimonial` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(180) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `usertype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `resume` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apartment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'kknderi@gmail.com','[]','$2y$13$.Z5dtwqaLN1IS4tzUD1/y.sWaWRXwDEofDgsp74TFyUaxo7b1EjhG',0,'admin',NULL,NULL,NULL,NULL,'Crystal','Shea','0114797473',NULL,'',NULL,NULL,NULL,NULL),(2,'maestrojosiah@gmail.com','[]','$2y$13$1s1FX/A83zeJQYpkChK7NuOrs14p09NhK6YsqbDWR7fGiSEGuy3Ta',0,'normal',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'milcahkelly6@gmail.com','[]','$2y$13$zU/IZ.8e02eTUsiLspGwa.1zJ2YZ0QbQt88ot2DBsw0v/fFdjbk0y',0,'normal',NULL,NULL,NULL,NULL,'Milcah','Kelly','0702825682',NULL,NULL,NULL,'Uganda Street House no. 22','Kiamumbi kiambu','');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_setting`
--

DROP TABLE IF EXISTS `user_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `setting_id` int DEFAULT NULL,
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C779A692A76ED395` (`user_id`),
  KEY `IDX_C779A692EE35BD72` (`setting_id`),
  CONSTRAINT `FK_C779A692A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_C779A692EE35BD72` FOREIGN KEY (`setting_id`) REFERENCES `setting` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_setting`
--

LOCK TABLES `user_setting` WRITE;
/*!40000 ALTER TABLE `user_setting` DISABLE KEYS */;
INSERT INTO `user_setting` VALUES (11,1,9,'Hair Products'),(12,1,12,'Crystal Shea'),(13,1,2,'/dist/assets/resume/16328075771.jpg'),(14,1,1,'/dist/assets/resume/16300785461.jpg'),(15,1,10,'Crystal Shea brings you natural products handcrafted with natural butters , cold pressed pure vegetable oils and essential oils.'),(16,1,8,'Natural products, natural beauty'),(17,1,6,'Handcrafted natural and organic'),(18,1,7,'skincare and haircare products'),(19,1,9,'Treat Your Body Better'),(20,1,11,'Our range of products includes, whipped body butters, Shea butter lotion, mineral sunscreen lotion, facial oil, body oil, hair butter, hair oil , natural lipbalms , black liquid soap and many more.'),(21,1,5,'/dist/assets/resume/16328077991.jpg'),(22,1,17,'Treat your body better'),(23,1,18,'Shop Now For Natural Products'),(24,1,16,'Let\'s Shop'),(25,1,3,'6am - 7pm'),(26,1,3,'Monday to Saturday - 6am - 7pm'),(27,1,19,'/dist/assets/resume/16328253611.jpg'),(28,1,20,'/dist/assets/resume/16328253791.jpg'),(29,1,21,'/dist/assets/resume/16329050501.jpg');
/*!40000 ALTER TABLE `user_setting` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-21 10:09:03
